#include <GL/glew.h>

#include <GL/glfw.h>

#include <math.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
using namespace glm;

#include "controls.hpp"

//Position
glm::vec3 position = glm::vec3(0,100,0);

//Frustum numbers
static float nearDist = 0.1f;
static float farDist = 3000.0f;

//Horizontal angle : toward -Z
float horizontalAngle = 2.0f * 3.14159f + (3.14159f/4.0f);
//Vertical angle : 0
float verticalAngle = -0.1f;


float initialFoV = 45.0f;

float speed = 100.0f;
float mouseSpeed = 0.005f;

bool houseToggle = true;

glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;


PlaneFrustum planefrustum;
void computeMatricesFromInputs(){

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);


	GLFWvidmode windowStruct;
	glfwGetDesktopMode(&windowStruct);

	int height = windowStruct.Height;
	int width = windowStruct.Width;

	height = 768;
	width = 1024;

	//Get mouse pos
	int xPos, yPos;
	glfwGetMousePos(&xPos, &yPos);

	//Centralize mouse pos
	glfwSetMousePos(width/2, height/2);

	//Get horizontal and vertical difference
	horizontalAngle += mouseSpeed * float(width/2 - xPos);
	verticalAngle += mouseSpeed * float(height/2 - yPos);

	if(verticalAngle < -3.1415/2){
		verticalAngle = -3.1415/2;
	}
	if(verticalAngle > 3.1415/2){
		verticalAngle = 3.1415/2;
	}

	//Angle sorcery
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
		);

	normalize(direction);

	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.1415f/2.0f),
		0.0f,
		cos(horizontalAngle - 3.1415f/2.0f)
		);

	normalize(right);

	glm::vec3 up = glm::cross(right, direction);
	normalize(up);

	//Position changes
	if(glfwGetKey(GLFW_KEY_UP) == GLFW_PRESS){
		position += direction * deltaTime * speed;
	}
	if(glfwGetKey(GLFW_KEY_DOWN) == GLFW_PRESS){
		position -= direction * deltaTime * speed;
	}
	if(glfwGetKey(GLFW_KEY_RIGHT) == GLFW_PRESS){
		position += right * deltaTime * speed;
	}
	if(glfwGetKey(GLFW_KEY_LEFT) == GLFW_PRESS){
		position -= right * deltaTime * speed;
	}
	if(glfwGetKey(GLFW_KEY_HOME) == GLFW_PRESS){
		houseToggle = !houseToggle;
	}

	float FoV = initialFoV - 5 * glfwGetMouseWheel();

	ProjectionMatrix = glm::perspective(FoV, float(width / height), nearDist, farDist);
	ViewMatrix = glm::lookAt(
		position,
		position + direction,
		up
		);


	lastTime = currentTime;

	//Make view frustum planes

	float Hnear = 2 * tan(FoV / 2) * nearDist;
	float Wnear = Hnear * float(width / height);

	float Hfar = 2 * tan(FoV / 2) * farDist;
	float Wfar = Hfar * float(width / height);

	glm::vec3 fc = position + direction * farDist;


	glm::vec3 nc = position + direction * nearDist;


	planefrustum.planes[0].point = nc;
	planefrustum.planes[0].n = direction;

	planefrustum.planes[1].point = fc;
	planefrustum.planes[1].n = -direction;

	glm::vec3 a = (nc + right * (Wnear / 2)) - position;
	normalize(a);
	glm::vec3 rightNormal = glm::cross(up , a);

	planefrustum.planes[2].point = position;
	planefrustum.planes[2].n = rightNormal;

	a = (nc - right * (Wnear / 2)) - position;
	normalize(a);
	glm::vec3 leftNormal = - glm::cross(up , a);

	planefrustum.planes[3].point = position;
	planefrustum.planes[3].n = leftNormal;

	a = (nc + up * (Hnear / 2)) - position;
	normalize(a);
	glm::vec3 topNormal = - glm::cross(right , a);

	planefrustum.planes[4].point = position;
	planefrustum.planes[4].n = topNormal;

	a = (nc - up * (Hnear / 2)) - position;
	normalize(a);
	glm::vec3 botNormal = glm::cross(right , a);

	planefrustum.planes[5].point = position;
	planefrustum.planes[5].n = botNormal;
}

PlaneFrustum getPlaneFrustum(){
	return planefrustum;
}

glm::mat4 getProjectionMatrix(){
	return ProjectionMatrix;
}

glm::mat4 getViewMatrix(){
	return ViewMatrix;
}

bool getHouseToggle(){
	return houseToggle;
}