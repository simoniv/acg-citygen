#ifndef CONTROLS_HPP
#define CONTROLS_HPP

class Plane{
	public:
		glm::vec3 point;
		glm::vec3 n;

		int distance(glm::vec3 r){
			return glm::dot(n, r) - glm::dot(n, point);
		}

		Plane::Plane(glm::vec3 p1, glm::vec3 n1){
			point = p1;
			n = n1;
		}
		Plane::Plane(){}

};

struct PlaneFrustum{
	Plane planes[6];
};

PlaneFrustum getPlaneFrustum();
void computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();
bool getHouseToggle();

#endif