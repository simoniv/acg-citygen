#include "AStar.h"

#include <set>
#include "../io/ConsolePrinter.h"

/**
 * A* algorithm for making the main roads
 */
std::vector<glm::vec2> AStar::aStar(glm::vec2 start, glm::vec2 goal, std::vector<std::vector<GridElement>> &grid, bool ignoreBuiltRoads) {
	//std::cout << "I am a aStar\nStart:"<< start.x << ":" << start.y << "\nGoal:" << goal.x << ":" << goal.y << "\n";
	//int consideredNodes = 0;
	std::set<glm::vec2, vec2_compare> openSet;
	std::vector<std::vector<bool>> helpOpenVector(grid.size(), std::vector<bool>(grid.size()));
	std::vector<std::vector<bool>> helpClosedVector(grid.size(), std::vector<bool>(grid.size()));

	openSet.insert(start);
	helpOpenVector[start.x][start.y] = true;
	std::map<glm::vec2, glm::vec2, vec2_compare> cameFrom;



	std::vector<std::vector<float>> gscore(grid.size(), std::vector<float>(grid.size()));
	//std::map<glm::vec2, float, vec2_compare> gscore;
	//gscore.insert(std::pair<glm::vec2,float>(start,0.0f));
	gscore[start.x][start.y] = 0.0f;

	std::vector<std::vector<float>> fscore(grid.size(), std::vector<float>(grid.size()));
	//std::map<glm::vec2, float, vec2_compare> fscore;
	//fscore.insert(std::pair<glm::vec2,float>(start, (float)costEstimator(start, goal)));
	fscore[start.x][start.y] = (float)costEstimator(start, goal);
	while (openSet.size() > 0) {
		//consideredNodes++;



		glm::vec2 currentNode = *openSet.begin();
		float currentLowest = fscore[currentNode.x][currentNode.y];

		for (std::set<glm::vec2, vec2_compare>::iterator it = openSet.begin(); it != openSet.end(); ++it) {
			float checkFloat = fscore[(*it).x][(*it).y];


			if (checkFloat < currentLowest) {
				currentLowest = checkFloat;
				currentNode = *it;
			}

		}
		if (currentNode.x == goal.x && currentNode.y == goal.y) {
			//std::cout << "Nodes considered: " << consideredNodes << "\n";
			return reconstructPath(cameFrom, goal);
		}
		//std::cout << "No reconstruct\n";
		openSet.erase(currentNode);
		helpOpenVector[currentNode.x][currentNode.y] = false;
		helpClosedVector[currentNode.x][currentNode.y] = true;

		//std::cout << "Visited:" << currentNode.x << ":" << currentNode.y <<"\n";
		for (int i = currentNode.x - 1; i <= currentNode.x + 1; i++) {
			for (int j = currentNode.y - 1; j <= currentNode.y + 1; j++) {
				if (i != -1 && j != -1 && i != grid.size() && j != grid.size()) {

					glm::vec2 neighbor = glm::vec2(i, j);
					//std::cout << "--Considered:" << neighbor.x << ":" << neighbor.y << "\n";
					float nodeDist = 4.0f;
					bool gscoreHigher = false;

					if ((grid[i][j].height - grid[currentNode.x][currentNode.y].height) > 1) {
						nodeDist = 16.0f;
						//std::cout << "Penalized";
					}
					if ((grid[i][j].height - grid[currentNode.x][currentNode.y].height) > 5) {
						nodeDist = 32.0f;
					}
					if (!ignoreBuiltRoads) {
						if (grid[i][j].mainAreaType == ROAD || grid[i][j].mainAreaType == HIGHWAY) {
							nodeDist = 1.0f;
						}
					}
					float tentativegscore = gscore[currentNode.x][currentNode.y] + nodeDist;
					if (helpClosedVector[neighbor.x][neighbor.y]) {
						if (tentativegscore >= gscore[neighbor.x][neighbor.y]) {
							continue;
						}
						else {
							gscoreHigher = true;
						}
					}


					//std::set<glm::vec2, vec2_compare>::iterator neighborIterator = openSet.find(neighbor); neighborIterator == openSet.end()
					if (!helpOpenVector[neighbor.x][neighbor.y] || gscoreHigher) {
						cameFrom.insert(std::pair<glm::vec2, glm::vec2>(neighbor, currentNode));
						gscore[neighbor.x][neighbor.y] = tentativegscore;
						fscore[neighbor.x][neighbor.y] = tentativegscore + (float)costEstimator(neighbor, goal);


						//gscore.insert(std::pair<glm::vec2, float>(neighbor, tentativegscore));
						//fscore.insert(std::pair<glm::vec2, float>(neighbor, tentativegscore + (float)costEstimator(neighbor,goal)));
						if (!helpOpenVector[neighbor.x][neighbor.y]) {
							openSet.insert(neighbor);
							helpOpenVector[neighbor.x][neighbor.y] = true;

						}
						//std::cout << "Visited:" << neighbor.x << ":" << neighbor.y << "\n";
					}



				}

			}
		}

		//std::cout << "Pushed through\n";
	}

	//std::cout << "Failed finding path between: " << start.x << ":" << start.y << " and " << goal.x << ":" << goal.y;
	ConsolePrinter::printString("Failed finding path between: " + std::to_string(start.x) + ":" + std::to_string(start.y) + " and " + std::to_string(goal.x) + ":" + std::to_string(goal.y));
	std::vector<glm::vec2> retvec;

	return retvec;

}

/**
 * Help method for the A* algorithm. Gives the manhattan distance between two coordinates
 */
int AStar::costEstimator(glm::vec2 start, glm::vec2 goal) {
	return abs(start.x - goal.x) + abs(start.y - goal.y);

}

/**
 * Help method for the A* algorithm. Reconstructs the path once the goal has been achieved
 */
std::vector<glm::vec2> AStar::reconstructPath(std::map<glm::vec2, glm::vec2, vec2_compare> &cameFrom, glm::vec2 currentNode) {
	if (cameFrom.find(currentNode) != cameFrom.end()) {
		std::vector<glm::vec2> returnVec = reconstructPath(cameFrom, (*cameFrom.find(currentNode)).second);
		returnVec.push_back(currentNode);
		return returnVec;
	}
	else {
		std::vector<glm::vec2> returnVec;
		returnVec.push_back(currentNode);
		return returnVec;
	}
}