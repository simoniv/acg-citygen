#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>

//Enum describing types of areas.
enum AreaType : int
{
	WILDERNESS = 1
	, CITYAREA = 2
	, INDUSTRIALAREA = 4
	, PARKAREA = 8
	, RESIDENTIALAREA = 16
	, ROAD = 32
	, HIGHWAY = 64
	, ROADRESERVED = 128
	, HIGHWAYRESERVED = 256
};

// AreaType to be treated as flags
inline AreaType operator~ (AreaType a) { return (AreaType)~static_cast<int>(a); }
inline AreaType operator| (AreaType a, AreaType b) { return (AreaType)(static_cast<int>(a) | static_cast<int>(b)); }
inline AreaType operator& (AreaType a, AreaType b) { return (AreaType)(static_cast<int>(a) & static_cast<int>(b)); }
inline AreaType operator^ (AreaType a, AreaType b) { return (AreaType)(static_cast<int>(a) ^ static_cast<int>(b)); }

// Can't do, can be done in a hacky way using reinterpret, but for now using the "long" way of for example a = a | b...
//inline AreaType& operator|= (AreaType& a, AreaType b) { return (AreaType&)(static_cast<int&>(a) |= static_cast<int>(b)); }
//inline AreaType& operator&= (AreaType& a, AreaType b) { return (AreaType&)(static_cast<int&>(a) &= static_cast<int>(b)); }
//inline AreaType& operator^= (AreaType& a, AreaType b) { return (AreaType&)(static_cast<int&>(a) ^= static_cast<int>(b)); }

class GridElement
{
public:

	GridElement();

	glm::vec3 getTopTriangleNormal();

	glm::vec3 getLeftTriangleNormal();

	glm::vec3 getRightTriangleNormal();

	glm::vec3 getBotTriangleNormal();

	bool mainAreaIsAny(AreaType types);

	bool mainAreaIsNot(AreaType types);

	float height;
	bool isCityCenter;
	bool isIndustrialCenter;
	int region;
	glm::vec3* topLeftCoord;
	glm::vec3* topRightCoord;
	glm::vec3* botLeftCoord;
	glm::vec3* botRightCoord;
	glm::vec3* midCoord;
	bool* topEdgeToggle;
	bool* botEdgeToggle;
	bool* leftEdgeToggle;
	bool* rightEdgeToggle;
	bool nwEdgeToggle;
	bool neEdgeToggle;
	bool swEdgeToggle;
	bool seEdgeToggle;
	AreaType mainAreaType;
	AreaType topTriangleAreaType;
	AreaType botTriangleAreaType;
	AreaType leftTriangleAreaType;
	AreaType rightTriangleAreaType;
	int topTriangleParcelNumber;
	int botTriangleParcelNumber;
	int leftTriangleParcelNumber;
	int rightTriangleParcelNumber;
	bool isChecked;
	bool isNextToRoad;
};