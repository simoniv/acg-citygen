#include "GridElement.h"


GridElement::GridElement() :
	height(0),
	isCityCenter(false),
	isIndustrialCenter(false),
	region(0),
	nwEdgeToggle(false),
	neEdgeToggle(false),
	swEdgeToggle(false),
	seEdgeToggle(false),
	mainAreaType(WILDERNESS),
	topTriangleAreaType(WILDERNESS),
	botTriangleAreaType(WILDERNESS),
	leftTriangleAreaType(WILDERNESS),
	rightTriangleAreaType(WILDERNESS),
	topTriangleParcelNumber(0),
	botTriangleParcelNumber(0),
	leftTriangleParcelNumber(0),
	rightTriangleParcelNumber(0),
	isChecked(false),
	isNextToRoad(false)
{

}

glm::vec3 GridElement::getTopTriangleNormal()
{
	glm::vec3 edge1 = *topRightCoord - *midCoord;
	glm::vec3 edge2 = *topLeftCoord - *midCoord;
	glm::vec3 normal = glm::cross(edge1, edge2);
	glm::normalize(normal);
	return normal;
};

glm::vec3 GridElement::getLeftTriangleNormal()
{
	glm::vec3 edge1 = *botLeftCoord - *midCoord;
	glm::vec3 edge2 = *topLeftCoord - *midCoord;
	glm::vec3 normal = glm::cross(edge2, edge1);
	glm::normalize(normal);
	return normal;
};

glm::vec3 GridElement::getRightTriangleNormal()
{
	glm::vec3 edge1 = *botRightCoord - *midCoord;
	glm::vec3 edge2 = *topRightCoord - *midCoord;
	glm::vec3 normal = glm::cross(edge1, edge2);
	glm::normalize(normal);
	return normal;
};

glm::vec3 GridElement::getBotTriangleNormal()
{
	glm::vec3 edge1 = *botRightCoord - *midCoord;
	glm::vec3 edge2 = *botLeftCoord - *midCoord;
	glm::vec3 normal = glm::cross(edge2, edge1);
	glm::normalize(normal);
	return normal;
};

bool GridElement::mainAreaIsAny(AreaType types)
{
	AreaType hit = mainAreaType & types;
	if (static_cast<int>(hit) == 0)
	{
		return false;
	}
	return true;
}


bool GridElement::mainAreaIsNot(AreaType types)
{
	AreaType hit = mainAreaType & types;
	if (static_cast<int>(hit) != 0)
	{
		return false;
	}
	return true;
}