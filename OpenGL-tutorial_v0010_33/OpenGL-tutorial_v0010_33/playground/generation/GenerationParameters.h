//Dimensions of the coarse grid. Representing heightfields et al.
const int GRIDWIDTH = 257;
const int GRIDLENGTH = 257;

//Dimensions of the fine grid that the rendered model will be based on.
const int FINEGRIDWIDTH = 1285;
const int FINEGRIDLENGTH = 1285;

//The resolution of the main roads grid.
const int MAINSIZE = 25;

//Default heightfield height.
const int DEFAULTHEIGHT = 0;

//Height boundaries a city center can exist within.
const float MAXCITYALTITUDE = 0.8f;
const float MINCITYALTITUDE = 0.0f;

//Min distance between main city and all other points.
const float CITYCENTERSPREAD = 20.0f;

//Number of city centers.
const int CITYCENTERPOINTS = 20;