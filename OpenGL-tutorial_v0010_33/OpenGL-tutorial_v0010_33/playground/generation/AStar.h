#pragma once

#include <vector>
#include <map>

#include <glm/glm.hpp>

#include "GridElement.h"


//glm::vec2 comparator. Needed for the aStar algorithm.
struct vec2_compare {
	bool operator() (const glm::vec2& lhs, const glm::vec2& rhs) const {
		if (lhs.x < rhs.x) {
			return true;
		}
		else if (lhs.x == rhs.x) {
			if (lhs.y < rhs.y) {
				return true;
			}
		}

		return false;
	}
};

class AStar
{
public:
	static std::vector<glm::vec2> aStar(glm::vec2 start, glm::vec2 goal, std::vector<std::vector<GridElement>> &grid, bool ignoreBuiltRoads);
	static int costEstimator(glm::vec2 start, glm::vec2 goal);
	static std::vector<glm::vec2> reconstructPath(std::map<glm::vec2, glm::vec2, vec2_compare> &cameFrom, glm::vec2 currentNode);
};