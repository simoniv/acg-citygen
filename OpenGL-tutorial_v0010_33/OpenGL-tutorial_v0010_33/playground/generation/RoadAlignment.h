#pragma once

//Direction enum for minor roads
enum Direction { STRAIGHT, DIAGONAL };

//Support struct for the generation of minor roads.
struct RoadAlignment
{
	Direction direction;
	int majorRes;
	int minorRes;
};