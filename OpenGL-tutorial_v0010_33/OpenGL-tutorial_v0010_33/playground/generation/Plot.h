#pragma once

#include <vector>

#include "GridElement.h"

enum HouseType { NONE = 0, HOUSE, APARTMENTS_LARGE, APARTMENTS_SMALL, APARTMENT_RING, MALL, SCHOOL, OFFICE, SKYSCRAPER, INDUSTRY };

//Struct for information about plots
struct Plot {
	HouseType housetype;
	std::vector<GridElement*> elementVector;
	Plot::Plot(HouseType type, std::vector<GridElement*> plotElements)
	{
		housetype = type;
		elementVector = plotElements;
	};
};