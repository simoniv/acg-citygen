#pragma once

//Support struct for the generation of main roads.
struct RoadRelationElement
{
	bool isRoadNode;
	bool leftVisited;
	bool rightVisited;
	bool upVisited;
	bool downVisited;
	int x;
	int y;

	bool isAllVisited()
	{
		return leftVisited && rightVisited && upVisited && downVisited;
	}
};