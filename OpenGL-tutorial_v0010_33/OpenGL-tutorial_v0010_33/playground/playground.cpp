#include "playground.h"
#include "render/Chunk.h"
#include "render/Houses.h"
#include "render/PlaneMesh.h"
#include "render/Water.h"
#include "generation/GenerationParameters.h"
#include "generation/RoadAlignment.h"
#include "generation/RoadRelationElement.h"
#include "generation/AStar.h"
#include "io/ConsolePrinter.h"

const int MAXDEPTHRECURSION = 1000;

std::vector<HouseType> regionHouseTypes;

//Pi
const float PI = 3.41459265359;


int main(void)
{
	Citygen city;
	city.run();
	return 0;
}

int Citygen::run(void)
{
	ConsolePrinter::printString("Program started.");


	srand(time(NULL));
	int errCheck = setupWindow();

	if (errCheck == -1) {
		return -1;
	}

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	GLenum err = glGetError();

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);


	//Setup grids
	std::vector<std::vector<GridElement>> coarseGrid(GRIDWIDTH, std::vector<GridElement>(GRIDLENGTH));
	std::vector<std::vector<GridElement>> fineGrid(FINEGRIDWIDTH, std::vector<GridElement>(FINEGRIDLENGTH));
	std::deque<std::vector<glm::vec2>> highways;
	std::deque<std::vector<glm::vec2>> mainRoads;
	std::vector<glm::vec2> centerPoints;
	std::vector<Plot> plots;
	ConsolePrinter::printString("Reserved grids.");

	/*
	std::vector<glm::vec3> plane;
	plane.reserve(12*FINEGRIDWIDTH*FINEGRIDWIDTH);
	std::vector<glm::vec3> planecolors;
	planecolors.reserve(12*FINEGRIDWIDTH*FINEGRIDWIDTH);
	std::vector<glm::vec3> planenormals;
	planenormals.reserve(12*FINEGRIDWIDTH*FINEGRIDWIDTH);
	std::cout << "reserved plane\n";
	*/

	Houses houseObject;
	PlaneMesh planeMeshObject;
	Water waterObject = Water(0.0f - 0.5f, 0.0f - 0.5f, FINEGRIDWIDTH + 0.5f, FINEGRIDWIDTH + 0.5f);

	generateTerrain(coarseGrid);

	generateCenters(coarseGrid, centerPoints, CITYCENTERPOINTS);

	generateHighway(coarseGrid, centerPoints, highways);

	interpolateGrid(fineGrid, coarseGrid, centerPoints);

	bezierGrid(fineGrid, highways, 4, HIGHWAY);

	allotAreas(fineGrid, centerPoints);

	generateMainRoads(fineGrid, coarseGrid, centerPoints, mainRoads);

	bezierGrid(fineGrid, mainRoads, 1, ROAD);

	generateMinorRoads(fineGrid);

	makeTriangleRepresentation(fineGrid);

	waterFix(fineGrid);

	makePlots(fineGrid, plots);

	planeMeshObject.generateChunks(fineGrid);

	houseObject.generateChunks(plots);

	ConsolePrinter::printString("All systems are optimal, starting to render...");
	ConsolePrinter::printString("Triangle Count: " + std::to_string(Chunk::triangleCount));
	//while(true){}

	//std::Vectors containing vertice data.
	//std::vector<glm::vec3> plane;


	//std::cout << "Generating vertices... ";
	//makeVertices(plane, planecolors, planenormals, fineGrid);
	//std::cout << "Done!\n";



	//Init vertex array object
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	Chunk::programID = LoadShaders("shaders/vertexShader.vert", "shaders/fragmentShader.frag");

	GLuint MatrixID = glGetUniformLocation(Chunk::programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(Chunk::programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(Chunk::programID, "M");


	//Light settings
	glUseProgram(Chunk::programID);

	float piOfDay = PI / 2;

	//Time of day from 1 = midday to -1 = midnight
	float timeOfDay = sin(piOfDay);

	GLuint LightID = glGetUniformLocation(Chunk::programID, "LightPosition_worldspace");
	GLuint DaylightID = glGetUniformLocation(Chunk::programID, "DaylightDirection_worldspace");
	GLuint LightColor = glGetUniformLocation(Chunk::programID, "LightColor");
	GLuint LightPower = glGetUniformLocation(Chunk::programID, "LightPower");
	GLuint timeOfDayID = glGetUniformLocation(Chunk::programID, "timeOfDay");

	glm::vec3 daylightDir = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 lightPos = glm::vec3(FINEGRIDWIDTH / 2, FINEGRIDWIDTH, FINEGRIDLENGTH / 2);
	glm::mat4 lightPosTranslator = glm::mat4(1.0f, 0.0f, 0.0f, FINEGRIDWIDTH / 2,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, FINEGRIDLENGTH / 2,
		0.0f, 0.0f, 0.0f, 1.0f);
	glm::mat4 lightPosNegTranslator = glm::mat4(1.0f, 0.0f, 0.0f, -FINEGRIDWIDTH / 2,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, -FINEGRIDLENGTH / 2,
		0.0f, 0.0f, 0.0f, 1.0f);

	// TODO: Not hardcode...
	glfwSetMousePos(1024 / 2, 768 / 2);
	do {

		//Get perspective matrix
		computeMatricesFromInputs();
		glm::mat4 Projection = getProjectionMatrix();
		glm::mat4 View = getViewMatrix();
		glm::mat4 Model = glm::mat4(1.0);
		glm::mat4 MVP = Projection * View * Model;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(Chunk::programID);

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &Model[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &View[0][0]);

		//Set lights

		if (glfwGetKey(GLFW_KEY_PAGEUP) == GLFW_PRESS) {
			glm::vec4 tempTransVec = (glm::vec4(lightPos.x, lightPos.y, lightPos.z, 1) * lightPosNegTranslator);
			lightPos.x = tempTransVec.x;
			lightPos.y = tempTransVec.y;
			lightPos.z = tempTransVec.z;

			float rotationfactor = PI / 500.0f;
			piOfDay += rotationfactor;
			timeOfDay = sin(piOfDay);

			glm::mat3 rotationMatrix = glm::mat3(1.0f, 0.0f, 0.0f, 0.0f, cos(rotationfactor), -sin(rotationfactor), 0, sin(rotationfactor), cos(rotationfactor));
			lightPos = lightPos * rotationMatrix;
			daylightDir = daylightDir * rotationMatrix;

			tempTransVec = (glm::vec4(lightPos.x, lightPos.y, lightPos.z, 1) * lightPosTranslator);
			lightPos.x = tempTransVec.x;
			lightPos.y = tempTransVec.y;
			lightPos.z = tempTransVec.z;

		}
		if (glfwGetKey(GLFW_KEY_PAGEDOWN) == GLFW_PRESS) {
			glm::vec4 tempTransVec = (glm::vec4(lightPos.x, lightPos.y, lightPos.z, 1) * lightPosNegTranslator);
			lightPos.x = tempTransVec.x;
			lightPos.y = tempTransVec.y;
			lightPos.z = tempTransVec.z;

			float rotationfactor = -PI / 500.0f;
			piOfDay += rotationfactor;
			timeOfDay = sin(piOfDay);

			glm::mat3 rotationMatrix = glm::mat3(1.0f, 0.0f, 0.0f, 0.0f, cos(rotationfactor), -sin(rotationfactor), 0, sin(rotationfactor), cos(rotationfactor));
			lightPos = lightPos * rotationMatrix;
			daylightDir = daylightDir * rotationMatrix;

			tempTransVec = (glm::vec4(lightPos.x, lightPos.y, lightPos.z, 1) * lightPosTranslator);
			lightPos.x = tempTransVec.x;
			lightPos.y = tempTransVec.y;
			lightPos.z = tempTransVec.z;
		}
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);


		if (timeOfDay >= 0) {
			glUniform3f(DaylightID, daylightDir.x, daylightDir.y, daylightDir.z);

			glm::vec3 lightCol = glm::vec3(1, 1, 1);
			glUniform3f(LightColor, lightCol.x, lightCol.y, lightCol.z);

			float lightPow = glm::clamp(timeOfDay * 2, 0.0f, 1.0f);
			glUniform1f(LightPower, lightPow);
		}
		else {
			glm::vec3 nightTimeDir = daylightDir * glm::vec3(-1.0f, -1.0f, -1.0f);
			glUniform3f(DaylightID, nightTimeDir.x, nightTimeDir.y, nightTimeDir.z);

			glm::vec3 lightCol = glm::vec3(0.0f, 0.0f, 0.3f);
			glUniform3f(LightColor, lightCol.x, lightCol.y, lightCol.z);

			float lightPow = 0.1f*glm::clamp(-timeOfDay * 2, 0.0f, 1.0f);
			glUniform1f(LightPower, lightPow);
		}
		glUniform1f(timeOfDayID, timeOfDay);

		glClearColor(0.0f, 0.0f, 0.4f*glm::clamp(timeOfDay * 2, 0.0f, 1.0f), 0.0f);

		//Render stuff

		planeMeshObject.render();

		waterObject.render();

		if (getHouseToggle()) {
			houseObject.render();
		}

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);



		// Swap buffers
		glfwSwapBuffers();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(GLFW_KEY_ESC) != GLFW_PRESS &&
		glfwGetWindowParam(GLFW_OPENED));

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
	/*
	//Deletions
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteBuffers(1, &colorbuffer);
	*/

	//TODO: Fix deletion of all house VBO's
	glDeleteProgram(Chunk::programID);
	glDeleteVertexArrays(1, &VertexArrayID);

	return 0;
}

/**
 * Main window start
 */
int Citygen::setupWindow() {
	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	if (!glfwOpenWindow(1024, 768, 0, 0, 0, 0, 32, 0, GLFW_WINDOW))
	{
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return -1;
	}


	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	glfwSetWindowTitle("Citygen");

	// Ensure we can capture the escape key being pressed below
	glfwEnable(GLFW_STICKY_KEYS);

	return 1;


}

/**
 * Generate terrain using a Square-Diamond algorithm
 */
void Citygen::generateTerrain(std::vector<std::vector<GridElement>> &grid) {

	ConsolePrinter::printString("Generating height-field... ");

	int DATA_SIZE = grid.size();

	//Set initial height of the first four corner seeds.
	float h = 8.0f;

	grid[0][DATA_SIZE - 1].height = ((-h) + rand() * (h - (-h)) / RAND_MAX) * DEFAULTHEIGHT;
	grid[0][0].height = ((-h) + rand() * (h - (-h)) / RAND_MAX) * DEFAULTHEIGHT;
	grid[DATA_SIZE - 1][0].height = ((-h) + rand() * (h - (-h)) / RAND_MAX) * DEFAULTHEIGHT;
	grid[DATA_SIZE - 1][DATA_SIZE - 1].height = ((-h) + rand() * (h - (-h)) / RAND_MAX) * DEFAULTHEIGHT;

	//Set variation for the rest.
	h = 16.0f;

	//Standard Square-Diamond algorithm.
	for (int sideLength = DATA_SIZE - 1; sideLength >= 2; sideLength /= 2, h /= 2.0) {

		int halfSide = sideLength / 2;

		for (int x = 0; x < DATA_SIZE - 1; x += sideLength) {
			for (int y = 0; y < DATA_SIZE - 1; y += sideLength) {

				float avg = grid[x][y].height +
					grid[x + sideLength][y].height +
					grid[x][y + sideLength].height +
					grid[x + sideLength][y + sideLength].height;


				avg /= 4.0;


				float offset = ((-h) + rand() * (h - (-h)) / RAND_MAX) * (avg + 0.1);
				grid[x + halfSide][y + halfSide].height = avg + offset;

			}
		}



		for (int x = 0; x < DATA_SIZE - 1; x += halfSide) {
			for (int y = (x + halfSide) % sideLength; y < DATA_SIZE - 1; y += sideLength) {



				float avg =
					grid[(x - halfSide + DATA_SIZE) % DATA_SIZE][y].height +
					grid[(x + halfSide) % DATA_SIZE][y].height +
					grid[x][(y + halfSide) % DATA_SIZE].height +
					grid[x][(y - halfSide + DATA_SIZE) % DATA_SIZE].height;

				avg /= 4.0;


				float offset = ((-h) + rand() * (h - (-h)) / RAND_MAX) * (avg + 0.1);
				avg = avg + offset;


				grid[x][y].height = avg;


				if (x == 0) grid[DATA_SIZE - 1][y].height = avg;
				if (y == 0) grid[x][DATA_SIZE - 1].height = avg;
			}
		}
	}

	ConsolePrinter::printString("Done!");

}

/**
 * Sets random city and industrial centers on the map
 */
void Citygen::generateCenters(std::vector<std::vector<GridElement>> &grid, std::vector<glm::vec2> &centerPoints, int noOfPoints) {

	ConsolePrinter::printString("Generating city centers...");

	for (int i = 0; i < noOfPoints; i++) {

		//Randomize new points until one fits criteria.
		while (true) {
			int x = rand() % GRIDWIDTH;
			int y = rand() % GRIDLENGTH;
			bool breaker = false;

			//Is on too high or too low ground?
			if ((grid[x][y].height > MAXCITYALTITUDE) || (grid[x][y].height < MINCITYALTITUDE)) {
				continue;
			}

			//Is too close to "main part" of city. Possibly redundant due to size of map
			if ((i > 0) && ((((x - centerPoints[0].x)*(x - centerPoints[0].x)) + ((y - centerPoints[0].y)*(y - centerPoints[0].y))) < (CITYCENTERSPREAD * CITYCENTERSPREAD))) {
				continue;
			}

			//Do we have size to grow?
			for (int k = -2; k <= 2 && breaker == false; k++) {
				for (int j = -2; j <= 2 && breaker == false; j++) {
					if (x + k < 0 || y + j < 0 || x + k > grid.size() - 1 || y + j > grid.size() - 1) {
						continue;
					}


					if (grid[x + k][y + j].height > MAXCITYALTITUDE || grid[x + k][y + j].height < MINCITYALTITUDE) {
						breaker = true;
					}

				}

			}

			if (breaker) {
				continue;
			}

			//Set centers. First one is always City center and a bit larger.
			if (i == 0) {
				grid[x][y].isCityCenter = true;
				centerPoints.push_back(glm::vec2(x, y));
			}
			else {
				int typeChoice = rand() % 2;
				if (typeChoice == 0) {
					grid[x][y].isCityCenter = true;
					centerPoints.push_back(glm::vec2(x, y));
				}
				else {
					grid[x][y].isIndustrialCenter = true;
					centerPoints.push_back(glm::vec2(x, y));
				}


			}


			break;

		}


	}

	ConsolePrinter::printString("Done!");

}

/**
 * Generates highways
 */
void Citygen::generateHighway(std::vector<std::vector<GridElement>> &grid, std::vector<glm::vec2> &cityCenters, std::deque<std::vector<glm::vec2>> &road) {

	std::vector<glm::vec2> tempRoad;

	//Generate highway between main city center and outlying city centers
	for (int i = 1; i < cityCenters.size(); i++) {
		tempRoad = AStar::aStar(cityCenters[0], cityCenters[i], grid, false);
		try {
			road.push_back(tempRoad);
		}
		catch (std::bad_alloc& ba) {
			ConsolePrinter::printString("Too many roads");
		}

		for (int j = 0; j < tempRoad.size(); j++) {
			grid[tempRoad[j].x][tempRoad[j].y].mainAreaType = HIGHWAY;
		}

		ConsolePrinter::printProgress("Generating highways ", round(((float)i / (float)cityCenters.size())*100.0f));

	}
	ConsolePrinter::printProgress("Generating highways ", 100);
	ConsolePrinter::progressFinished();
	ConsolePrinter::printString("Done!");
}

/**
 * Generates main roads
 */
void Citygen::generateMainRoads(
	std::vector<std::vector<GridElement>> &grid,
	std::vector<std::vector<GridElement>> &coarseGrid,
	std::vector<glm::vec2> &centerPoints,
	std::deque<std::vector<glm::vec2>> &road
) {

	RoadRelationElement mainRoadPoints[MAINSIZE][MAINSIZE] = {};
	std::vector<glm::vec2> tempRoad;
	glm::vec2 tempStartVec;
	glm::vec2 tempEndVec;
	int randX = 0;
	int randY = 0;
	int roadCounter = 0;
	int roadFinished = 0;

	int randOffset = 8;
	int midPointer = 5;

	//Check what the main area element is for each main-road grid cell
	for (int i = 0; i < MAINSIZE; i++) {
		for (int j = 0; j < MAINSIZE; j++) {
			grid[(i * 10 * 5)][(j * 10 * 5)].isCityCenter = true;

			float fractionInhabited = 0;
			float counter = 0;
			for (int x = (i * 10 * 5); x < ((i + 1) * 10 * 5); x++) {
				for (int y = (j * 10 * 5); y < ((j + 1) * 10 * 5); y++) {
					//std::cout << x << ":" << y << "\n";
					if (grid[x][y].mainAreaType != WILDERNESS) {
						fractionInhabited++;
					}
					counter++;
				}
			}

			fractionInhabited /= counter;
			if (fractionInhabited >= 0.4) {
				mainRoadPoints[i][j].isRoadNode = true;
				roadCounter++;
			}

		}
	}
	/*
	for(int i = 0; i < MAINSIZE; i++){
		for(int j = 0; j < MAINSIZE; j++){
			if(mainRoadPoints[i][j].isRoadNode){
				std::cout << "x";
			} else {
				std::cout << "0";
			}
		}
		std::cout << "\n";
	}
	*/
	//std::cout << "Finished first loop";

	//Make the main roads.
	for (int i = 0; i < MAINSIZE; i++) {
		for (int j = 0; j < MAINSIZE; j++) {
			//Is it a road node with other road nodes not already treated?
			if (mainRoadPoints[i][j].isRoadNode && !mainRoadPoints[i][j].isAllVisited()) {

				if ((mainRoadPoints[i][j].x == 0) && (mainRoadPoints[i][j].y == 0)) {
					if (i == (MAINSIZE - 1)) {
						randX = rand() % 6;
					}
					else {
						randX = midPointer + ((rand() % randOffset) - (randOffset / 2));
					}
					if (j == (MAINSIZE - 1)) {
						randY = rand() % 6;
					}
					else {
						randY = midPointer + ((rand() % randOffset) - (randOffset / 2));
					}
					mainRoadPoints[i][j].x = (i * 10) + randX;
					mainRoadPoints[i][j].y = (j * 10) + randY;
					tempStartVec = glm::vec2(i * 10 + randX, j * 10 + randY);
				}
				else {
					tempStartVec = glm::vec2(mainRoadPoints[i][j].x, mainRoadPoints[i][j].y);
				}


				//NorthBound
				if ((i - 1) >= 0) {
					if (mainRoadPoints[i - 1][j].isRoadNode && !mainRoadPoints[i][j].upVisited) {
						if ((mainRoadPoints[i - 1][j].x == 0) && (mainRoadPoints[i - 1][j].y == 0)) {
							if ((i - 1) == (MAINSIZE - 1)) {
								randX = rand() % 6;
							}
							else {
								randX = midPointer + ((rand() % randOffset) - (randOffset / 2));
							}
							if (j == (MAINSIZE - 1)) {
								randY = rand() % 6;
							}
							else {
								randY = midPointer + ((rand() % randOffset) - (randOffset / 2));
							}
							mainRoadPoints[i - 1][j].x = (i - 1) * 10 + randX;
							mainRoadPoints[i - 1][j].y = j * 10 + randY;
							tempEndVec = glm::vec2((i - 1) * 10 + randX, j * 10 + randY);
						}
						else {
							tempEndVec = glm::vec2(mainRoadPoints[i - 1][j].x, mainRoadPoints[i - 1][j].y);
						}

						tempRoad = AStar::aStar(tempStartVec, tempEndVec, coarseGrid, true);
						road.push_back(tempRoad);
						mainRoadPoints[i - 1][j].downVisited = true;
						grid[tempStartVec.x * 5][tempStartVec.y * 5].isIndustrialCenter = true;
					}
				}

				//SouthBound
				if ((i + 1) < MAINSIZE) {
					if (mainRoadPoints[i + 1][j].isRoadNode && !mainRoadPoints[i][j].downVisited) {
						if ((mainRoadPoints[i + 1][j].x == 0) && (mainRoadPoints[i + 1][j].y == 0)) {
							if ((i + 1) == (MAINSIZE - 1)) {
								randX = rand() % 6;
							}
							else {
								randX = midPointer + ((rand() % randOffset) - (randOffset / 2));
							}
							if (j == (MAINSIZE - 1)) {
								randY = rand() % 6;
							}
							else {
								randY = midPointer + ((rand() % randOffset) - (randOffset / 2));
							}
							mainRoadPoints[i + 1][j].x = (i + 1) * 10 + randX;
							mainRoadPoints[i + 1][j].y = j * 10 + randY;
							tempEndVec = glm::vec2((i + 1) * 10 + randX, j * 10 + randY);
						}
						else {
							tempEndVec = glm::vec2(mainRoadPoints[i + 1][j].x, mainRoadPoints[i + 1][j].y);
						}

						tempRoad = AStar::aStar(tempStartVec, tempEndVec, coarseGrid, true);
						road.push_back(tempRoad);
						mainRoadPoints[i + 1][j].upVisited = true;
						grid[tempStartVec.x * 5][tempStartVec.y * 5].isIndustrialCenter = true;
					}
				}


				//WestBound
				if ((j - 1) >= 0) {
					if (mainRoadPoints[i][j - 1].isRoadNode && !mainRoadPoints[i][j].leftVisited) {
						if ((mainRoadPoints[i][j - 1].x == 0) && (mainRoadPoints[i][j - 1].y == 0)) {
							if (i == (MAINSIZE - 1)) {
								randX = rand() % 6;
							}
							else {
								randX = midPointer + ((rand() % randOffset) - (randOffset / 2));
							}
							if ((j - 1) == (MAINSIZE - 1)) {
								randY = rand() % 6;
							}
							else {
								randY = midPointer + ((rand() % randOffset) - (randOffset / 2));
							}
							mainRoadPoints[i][j - 1].x = i * 10 + randX;
							mainRoadPoints[i][j - 1].y = (j - 1) * 10 + randY;
							tempEndVec = glm::vec2(i * 10 + randX, (j - 1) * 10 + randY);
						}
						else {
							tempEndVec = glm::vec2(mainRoadPoints[i][j - 1].x, mainRoadPoints[i][j - 1].y);
						}

						tempRoad = AStar::aStar(tempStartVec, tempEndVec, coarseGrid, true);
						road.push_back(tempRoad);
						mainRoadPoints[i][j - 1].rightVisited = true;
						grid[tempStartVec.x * 5][tempStartVec.y * 5].isIndustrialCenter = true;
					}
				}

				//EastBound
				if ((j + 1) < MAINSIZE) {
					if (mainRoadPoints[i][j + 1].isRoadNode && !mainRoadPoints[i][j].rightVisited) {
						if ((mainRoadPoints[i][j + 1].x == 0) && (mainRoadPoints[i][j + 1].y == 0)) {
							if (i == (MAINSIZE - 1)) {
								randX = rand() % 6;
							}
							else {
								randX = midPointer + ((rand() % randOffset) - (randOffset / 2));
							}
							if ((j + 1) == (MAINSIZE - 1)) {
								randY = rand() % 6;
							}
							else {
								randY = midPointer + ((rand() % randOffset) - (randOffset / 2));
							}
							mainRoadPoints[i][j + 1].x = i * 10 + randX;
							mainRoadPoints[i][j + 1].y = (j + 1) * 10 + randY;
							tempEndVec = glm::vec2(i * 10 + randX, (j + 1) * 10 + randY);
						}
						else {
							tempEndVec = glm::vec2(mainRoadPoints[i][j + 1].x, mainRoadPoints[i][j + 1].y);
						}

						tempRoad = AStar::aStar(tempStartVec, tempEndVec, coarseGrid, true);
						road.push_back(tempRoad);
						mainRoadPoints[i][j + 1].leftVisited = true;
						grid[tempStartVec.x * 5][tempStartVec.y * 5].isIndustrialCenter = true;
					}
				}

				if (mainRoadPoints[i][j].isRoadNode) {

					ConsolePrinter::printProgress("Generating Main roads...", round(((float)roadFinished / (float)roadCounter)*100.0f));
					roadFinished++;

				}


			}
		}
	}

	ConsolePrinter::printProgress("Generating Main roads...", 100);
	ConsolePrinter::progressFinished();
	ConsolePrinter::printString("Done!");

}

/**
 * Generates minor roads
 */
void Citygen::generateMinorRoads(std::vector<std::vector<GridElement>> &grid) {
	ConsolePrinter::printString("Making minor roads...");

	int regionCounter = 1;

	int majorRes = 11;
	int minorRes = 6;
	int x;
	int y;

	//Assign Regions
	for (int i = 0; i < grid.size(); i++) {
		for (int j = 0; j < grid.size(); j++) {
			if (!grid[i][j].isChecked) {
				if (grid[i][j].mainAreaIsNot(WILDERNESS | ROAD | HIGHWAY | HIGHWAYRESERVED))
				{
					floodFillArea(grid, i, j, regionCounter);
					regionCounter++;
				}
			}

		}
	}
	regionHouseTypes.reserve(regionCounter);

	//Assign dimensions and direction of grids.
	std::vector<RoadAlignment> alignmentVector(regionCounter);
	for (int i = 0; i < alignmentVector.size(); i++) {
		int alignment = rand() % 2;
		if (alignment == 0) {
			alignmentVector[i].direction = STRAIGHT;
		}
		else {
			alignmentVector[i].direction = DIAGONAL;
		}

		int minorRes = 6 + (rand() % 10);
		int majorRes = 6 + (rand() % 10);
		alignmentVector[i].majorRes = majorRes;
		alignmentVector[i].minorRes = minorRes;

	}


	//Grow the roads
	for (int i = 0; i < grid.size(); i++) {
		for (int j = 0; j < grid.size(); j++) {
			if (grid[i][j].isChecked && grid[i][j].mainAreaIsNot(ROAD | HIGHWAY))
			{
				if (alignmentVector[grid[i][j].region].direction == STRAIGHT) {
					if (i%alignmentVector[grid[i][j].region].majorRes == 0) {
						x = i;
						y = j;
						do {
							grid[x][y].mainAreaType = ROAD;
							y++;
							if (y >= grid.size()) {
								break;
							}
						} while (grid[x][y].mainAreaIsNot(ROAD | HIGHWAY) && grid[x][y].isChecked);
					}
					if (j%alignmentVector[grid[i][j].region].minorRes == 0) {
						x = i;
						y = j;
						do {
							grid[x][y].mainAreaType = ROAD;
							x++;
							if (x >= grid.size()) {
								break;
							}
						} while (grid[x][y].mainAreaIsNot(ROAD | HIGHWAY) && grid[x][y].isChecked);
					}
				}
				else { //Diagonal grid
					if ((i%alignmentVector[grid[i][j].region].majorRes) == (j%alignmentVector[grid[i][j].region].majorRes)) {
						x = i;
						y = j;
						do {
							grid[x][y].mainAreaType = ROAD;
							y++;
							x++;
							if (y >= grid.size() || x >= grid.size() || (grid[x - 1][y].mainAreaIsAny(ROAD | HIGHWAY) && grid[x][y - 1].mainAreaIsAny(ROAD | HIGHWAY)))
							{
								break;
							}
						} while (grid[x][y].mainAreaIsNot(ROAD | HIGHWAY) && grid[x][y].isChecked);
					}
					if (((j%alignmentVector[grid[i][j].region].minorRes) + (i%alignmentVector[grid[i][j].region].minorRes)) == alignmentVector[grid[i][j].region].minorRes) {
						x = i;
						y = j;
						do {
							grid[x][y].mainAreaType = ROAD;
							x--;
							y++;
							if ((x < 0) || (y >= grid.size()) || (grid[x + 1][y].mainAreaIsAny(ROAD | HIGHWAY) && grid[x][y - 1].mainAreaIsAny(ROAD | HIGHWAY)))
							{
								break;
							}
						} while (grid[x][y].mainAreaIsNot(ROAD | HIGHWAY) && grid[x][y].isChecked);

						x = i;
						y = j;
						do {
							grid[x][y].mainAreaType = ROAD;
							x++;
							y--;
							if ((y < 0) || (x >= grid.size()) || (grid[x - 1][y].mainAreaIsAny(ROAD | HIGHWAY) && grid[x][y + 1].mainAreaIsAny(ROAD | HIGHWAY)))
							{
								break;
							}
						} while (grid[x][y].mainAreaIsNot(ROAD | HIGHWAY) && grid[x][y].isChecked);
					}


				}

			}
		}
	}


	//cleanup
	for (int i = 0; i < grid.size(); i++) {
		for (int j = 0; j < grid.size(); j++) {
			grid[i][j].isChecked = false;
		}
	}
	ConsolePrinter::printString("Done!");

}

/**
 * Reserves the road margins for larger roads
 */
void Citygen::reserveRoadMargin(std::vector<std::vector<GridElement>> &grid, int x, int y, int size, AreaType type)
{
	for (int i = y - size; i <= y + size; i++)
	{
		if ((i >= 0) && (i < grid.size()) && grid[x][i].mainAreaIsNot(ROAD | HIGHWAY))
		{
			grid[x][i].mainAreaType = type;
		}
	}
	for (int i = x - size; i <= x + size; i++)
	{
		if ((i >= 0) && (i < grid.size()) && grid[i][y].mainAreaIsNot(ROAD | HIGHWAY))
		{
			grid[i][y].mainAreaType = type;
		}
	}
}

/**
 * Smoothes the curves of the larger roads
 */
void Citygen::bezierGrid(std::vector<std::vector<GridElement>> &fineGrid, std::deque<std::vector<glm::vec2>> &road, int roadReserveSize, AreaType type) {

	ConsolePrinter::printString("Smoothing the roads...");

	int magnifyingFactor = FINEGRIDWIDTH / GRIDWIDTH;
	AreaType reserveType;
	if (type == HIGHWAY) {
		reserveType = HIGHWAYRESERVED;
	}
	else {
		reserveType = ROADRESERVED;
	}

	/*
	std::vector<std::vector<glm::vec2>> roads(100000);
	int roadcounter = 0;

	for(int i = 1; i < road.size(); i = i++){

		//Is neighbour?
		if(
			((road[i].x == road[i-1].x) && (abs(road[i].y - road[i-1].y) == 1))
			||
			((road[i].y == road[i-1].y) && (abs(road[i].x - road[i-1].x) == 1))
			||
			((abs(road[i].x - road[i-1].x) == 1) && (abs(road[i].y - road[i-1].y) == 1))
			){
				//std::cout << "woot!";
				roads.at(roadcounter).push_back(road[i-1]);
				//std::cout << "Pushed: " << road[i-1].x << ":" << road[i-1].y << "\n";
		}
		else{
			roads.at(roadcounter).push_back(road[i-1]);
			//std::cout << "Pushed: " << road[i-1].x << ":" << road[i-1].y << "\nNew road\n";
			roadcounter++;

		}

	}
	*/
	//std::cout << "Passed division of roads";

	//Checks the road coordinates in chunks of three
	for (int k = 0; k < road.size(); k++) {
		for (int i = 2; i < road[k].size(); i = i + 2) {


			//If all road coordinates are in a row...
			if ((road[k][i - 2].x == road[k][i - 1].x) && (road[k][i - 1].x == road[k][i].x)) {
				int startVal;
				int endVal;
				if (road[k][i - 2].y*magnifyingFactor < road[k][i].y*magnifyingFactor) {
					startVal = road[k][i - 2].y*magnifyingFactor;
					endVal = road[k][i].y*magnifyingFactor;
				}
				else {
					startVal = road[k][i].y*magnifyingFactor;
					endVal = road[k][i - 2].y*magnifyingFactor;
				}

				for (int j = startVal; j < endVal; j++) {
					fineGrid[road[k][i].x*magnifyingFactor][j].mainAreaType = type;
					reserveRoadMargin(fineGrid, road[k][i].x*magnifyingFactor, j, roadReserveSize, reserveType);
				}

			}
			else if ((road[k][i - 2].y == road[k][i - 1].y) && (road[k][i - 1].y == road[k][i].y)) {

				int startVal;
				int endVal;
				if (road[k][i - 2].x*magnifyingFactor < road[k][i].x*magnifyingFactor) {
					startVal = road[k][i - 2].x*magnifyingFactor;
					endVal = road[k][i].x*magnifyingFactor;
				}
				else {
					startVal = road[k][i].x*magnifyingFactor;
					endVal = road[k][i - 2].x*magnifyingFactor;
				}

				for (int j = startVal; j < endVal; j++) {
					fineGrid[j][road[k][i].y*magnifyingFactor].mainAreaType = type;
					reserveRoadMargin(fineGrid, j, road[k][i].y*magnifyingFactor, roadReserveSize, reserveType);
				}

			}
			else { //Road coordinates not in a row, need bezier extrapolation
				for (float t = 0.0f; t < 1.0f; t += (1.0 / 50)) {
					float mt = 1 - t;
					int xcoord = round(mt*mt*road[k][i - 2].x*magnifyingFactor + 2 * mt*t*road[k][i - 1].x*magnifyingFactor + t * t*road[k][i].x*magnifyingFactor);
					int ycoord = round(mt*mt*road[k][i - 2].y*magnifyingFactor + 2 * mt*t*road[k][i - 1].y*magnifyingFactor + t * t*road[k][i].y*magnifyingFactor);
					fineGrid[xcoord][ycoord].mainAreaType = type;
					reserveRoadMargin(fineGrid, xcoord, ycoord, roadReserveSize, reserveType);


				}



			}

			//Fix for last bit of road when the length of the road is not evenly divisible by three
			if (i + 2 >= road[k].size()) {
				i = road[k].size() - 1;

				if ((road[k][i - 2].x == road[k][i - 1].x) && (road[k][i - 1].x == road[k][i].x)) {
					int startVal;
					int endVal;
					if (road[k][i - 2].y*magnifyingFactor < road[k][i].y*magnifyingFactor) {
						startVal = road[k][i - 2].y*magnifyingFactor;
						endVal = road[k][i].y*magnifyingFactor;
					}
					else {
						startVal = road[k][i].y*magnifyingFactor;
						endVal = road[k][i - 2].y*magnifyingFactor;
					}

					for (int j = startVal; j < endVal; j++) {
						fineGrid[road[k][i].x*magnifyingFactor][j].mainAreaType = type;
						reserveRoadMargin(fineGrid, road[k][i].x*magnifyingFactor, j, roadReserveSize, reserveType);
					}

				}
				else if ((road[k][i - 2].y == road[k][i - 1].y) && (road[k][i - 1].y == road[k][i].y)) {

					int startVal;
					int endVal;
					if (road[k][i - 2].x*magnifyingFactor < road[k][i].x*magnifyingFactor) {
						startVal = road[k][i - 2].x*magnifyingFactor;
						endVal = road[k][i].x*magnifyingFactor;
					}
					else {
						startVal = road[k][i].x*magnifyingFactor;
						endVal = road[k][i - 2].x*magnifyingFactor;
					}

					for (int j = startVal; j < endVal; j++) {
						fineGrid[j][road[k][i].y*magnifyingFactor].mainAreaType = type;
						reserveRoadMargin(fineGrid, j, road[k][i].y*magnifyingFactor, roadReserveSize, reserveType);
					}

				}
				else {
					for (float t = 0.0f; t < 1.0f; t += (1.0 / 50)) {
						float mt = 1 - t;
						int xcoord = round(mt*mt*road[k][i - 2].x*magnifyingFactor + 2 * mt*t*road[k][i - 1].x*magnifyingFactor + t * t*road[k][i].x*magnifyingFactor);
						int ycoord = round(mt*mt*road[k][i - 2].y*magnifyingFactor + 2 * mt*t*road[k][i - 1].y*magnifyingFactor + t * t*road[k][i].y*magnifyingFactor);
						fineGrid[xcoord][ycoord].mainAreaType = type;
						reserveRoadMargin(fineGrid, xcoord, ycoord, roadReserveSize, reserveType);


					}



				}



			}



		}
	}

	ConsolePrinter::printString("Done!");

}

/**
 * Make regions out of areas through flood-filling, to be used for making the smaller roads.
 */
void Citygen::floodFillArea(std::vector<std::vector<GridElement>> &grid, int x, int y, int regionCounter) 
{
	std::deque<std::pair<int, int>> floodedArea;
	floodedArea.emplace_back(std::pair<int, int>(x, y));

	while (!floodedArea.empty())
	{
		int currentX = floodedArea.front().first;
		int currentY = floodedArea.front().second;
		floodedArea.pop_front();

		if (grid[currentX][currentY].isChecked)
		{
			continue;
		}

		grid[currentX][currentY].isChecked = true;
		grid[currentX][currentY].region = regionCounter;

		if (((currentX - 1) >= 0) && !grid[currentX - 1][currentY].isChecked)
		{
			if (grid[currentX - 1][currentY].mainAreaIsNot(WILDERNESS | ROAD | HIGHWAY | HIGHWAYRESERVED))
			{
				floodedArea.emplace_back(std::pair<int,int>(currentX - 1, currentY));
			}
		}

		if (((currentX + 1) < grid.size()) && !grid[currentX + 1][currentY].isChecked)
		{
			if (grid[currentX + 1][currentY].mainAreaIsNot(WILDERNESS | ROAD | HIGHWAY | HIGHWAYRESERVED))
			{
				floodedArea.emplace_back(std::pair<int, int>(currentX + 1, currentY));
			}
		}

		if (((currentY - 1) >= 0) && !grid[currentX][currentY - 1].isChecked)
		{

			if (grid[currentX][currentY - 1].mainAreaIsNot(WILDERNESS | ROAD | HIGHWAY | HIGHWAYRESERVED))
			{
				floodedArea.emplace_back(std::pair<int, int>(currentX, currentY - 1));
			}
		}
		if (((currentY + 1) < grid.size()) && !grid[currentX][currentY + 1].isChecked)
		{
			if (grid[currentX][currentY + 1].mainAreaIsNot(WILDERNESS | ROAD | HIGHWAY | HIGHWAYRESERVED))
			{
				floodedArea.emplace_back(std::pair<int, int>(currentX, currentY + 1));
			}
		}
	}
}

/**
 * Make regions out of areas through flood-filling, to be used for parcellation
 */
void Citygen::floodFillHouseParcel(std::vector<std::vector<GridElement>> &grid, int x, int y, int regionCounter) {
	grid[x][y].isChecked = true;
	grid[x][y].isNextToRoad = false;
	//grid[x][y].region = regionCounter;

	if (grid[x][y].topTriangleAreaType == CITYAREA || grid[x][y].topTriangleAreaType == INDUSTRIALAREA || grid[x][y].topTriangleAreaType == RESIDENTIALAREA) {
		grid[x][y].topTriangleParcelNumber = regionCounter;
	}
	else {
		grid[x][y].topTriangleParcelNumber = -1;
	}

	if (grid[x][y].botTriangleAreaType == CITYAREA || grid[x][y].botTriangleAreaType == INDUSTRIALAREA || grid[x][y].botTriangleAreaType == RESIDENTIALAREA) {
		grid[x][y].botTriangleParcelNumber = regionCounter;
	}
	else {
		grid[x][y].botTriangleParcelNumber = -1;
	}

	if (grid[x][y].leftTriangleAreaType == CITYAREA || grid[x][y].leftTriangleAreaType == INDUSTRIALAREA || grid[x][y].leftTriangleAreaType == RESIDENTIALAREA) {
		grid[x][y].leftTriangleParcelNumber = regionCounter;
	}
	else {
		grid[x][y].leftTriangleParcelNumber = -1;
	}

	if (grid[x][y].rightTriangleAreaType == CITYAREA || grid[x][y].rightTriangleAreaType == INDUSTRIALAREA || grid[x][y].rightTriangleAreaType == RESIDENTIALAREA) {
		grid[x][y].rightTriangleParcelNumber = regionCounter;
	}
	else {
		grid[x][y].rightTriangleParcelNumber = -1;
	}


	if (((x - 1) >= 0)) {
		if (!grid[x - 1][y].isChecked && (grid[x - 1][y].rightTriangleAreaType != WILDERNESS) && grid[x - 1][y].rightTriangleAreaType != ROAD && grid[x - 1][y].rightTriangleAreaType != HIGHWAY && grid[x - 1][y].rightTriangleAreaType != ROADRESERVED && grid[x - 1][y].rightTriangleAreaType != HIGHWAYRESERVED) {
			floodFillHouseParcel(grid, x - 1, y, regionCounter);
		}
		if (grid[x - 1][y].rightTriangleAreaType == ROAD || grid[x - 1][y].rightTriangleAreaType == HIGHWAY || grid[x - 1][y].rightTriangleAreaType == ROADRESERVED || grid[x - 1][y].rightTriangleAreaType == HIGHWAYRESERVED) {
			grid[x][y].isNextToRoad = true;
		}
	}

	if (((x + 1) < grid.size())) {
		if (!grid[x + 1][y].isChecked && (grid[x + 1][y].leftTriangleAreaType != WILDERNESS) && grid[x + 1][y].leftTriangleAreaType != ROAD && grid[x + 1][y].leftTriangleAreaType != HIGHWAY && grid[x + 1][y].leftTriangleAreaType != ROADRESERVED && grid[x + 1][y].leftTriangleAreaType != HIGHWAYRESERVED) {
			floodFillHouseParcel(grid, x + 1, y, regionCounter);
		}
		if (grid[x + 1][y].leftTriangleAreaType == ROAD || grid[x + 1][y].leftTriangleAreaType == HIGHWAY || grid[x + 1][y].leftTriangleAreaType == ROADRESERVED || grid[x + 1][y].leftTriangleAreaType == HIGHWAYRESERVED) {
			grid[x][y].isNextToRoad = true;
		}
	}

	if (((y - 1) >= 0)) {
		if (!grid[x][y - 1].isChecked && (grid[x][y - 1].botTriangleAreaType != WILDERNESS) && grid[x][y - 1].botTriangleAreaType != ROAD && grid[x][y - 1].botTriangleAreaType != HIGHWAY && grid[x][y - 1].botTriangleAreaType != ROADRESERVED && grid[x][y - 1].botTriangleAreaType != HIGHWAYRESERVED) {
			floodFillHouseParcel(grid, x, y - 1, regionCounter);
		}
		if (grid[x][y - 1].botTriangleAreaType == ROAD || grid[x][y - 1].botTriangleAreaType == HIGHWAY || grid[x][y - 1].botTriangleAreaType == ROADRESERVED || grid[x][y - 1].botTriangleAreaType == HIGHWAYRESERVED) {
			grid[x][y].isNextToRoad = true;
		}
	}
	if (((y + 1) < grid.size())) {
		if (!grid[x][y + 1].isChecked && (grid[x][y + 1].topTriangleAreaType != WILDERNESS) && grid[x][y + 1].topTriangleAreaType != ROAD && grid[x][y + 1].topTriangleAreaType != HIGHWAY && grid[x][y + 1].topTriangleAreaType != ROADRESERVED && grid[x][y + 1].topTriangleAreaType != HIGHWAYRESERVED) {
			floodFillHouseParcel(grid, x, y + 1, regionCounter);
		}
		if (grid[x][y + 1].topTriangleAreaType == ROAD || grid[x][y + 1].topTriangleAreaType == HIGHWAY || grid[x][y + 1].topTriangleAreaType == ROADRESERVED || grid[x][y + 1].topTriangleAreaType == HIGHWAYRESERVED) {
			grid[x][y].isNextToRoad = true;
		}
	}
}

/**
 * Interpolates the coarse grid into a the fine grid, mainly height and city centers
 */
void Citygen::interpolateGrid(std::vector<std::vector<GridElement>> &fineGrid, std::vector<std::vector<GridElement>> &coarseGrid, std::vector<glm::vec2> &centerPoints) {

	ConsolePrinter::printString("Interpolating heightfield...");

	int coarseGridSize = coarseGrid.size();
	int fineGridSize = fineGrid.size();
	float ratio = fineGridSize / coarseGridSize;


	for (int x = 0; x < fineGridSize; x++) {
		for (int y = 0; y < fineGridSize; y++) {
			int lowX = floor(x / ratio);
			int highX = lowX + 1;
			if (highX == coarseGridSize) {
				highX = lowX;
			}

			int lowY = floor(y / ratio);
			int highY = lowY + 1;
			if (highY == coarseGridSize) {
				highY = lowY;
			}

			//Bilinear interpolation
			float xFrac = ((float)x / ratio) - lowX;
			float yFrac = ((float)y / ratio) - lowY;
			/*if(x > fineGridSize-9){
			std::cout << "Lowx:" <<lowX << " x:" << x << " x/grid:" << ((float)x /(float)coarseGridSize) << " YFrac:" << xFrac << "\n";
			std::cout << "Lowy:" <<lowY << " y:" << y << " y/grid:" << ((float)y /(float)coarseGridSize) << " YFrac:" << yFrac << "\n";
			}*/

			float interp1 = (coarseGrid[lowX][lowY].height)*(1 - xFrac) + (coarseGrid[highX][lowY].height)*(xFrac);
			float interp2 = (coarseGrid[lowX][highY].height)*(1 - xFrac) + (coarseGrid[highX][highY].height)*(xFrac);

			float finalInterp = interp1 * (1 - yFrac) + interp2 * (yFrac);

			fineGrid[x][y].height = finalInterp;


		}
	}

	//Set centers
	for (int i = 0; i < centerPoints.size(); i++) {
		fineGrid[centerPoints[i].x*ratio][centerPoints[i].y*ratio].isCityCenter = coarseGrid[centerPoints[i].x][centerPoints[i].y].isCityCenter;
		fineGrid[centerPoints[i].x*ratio][centerPoints[i].y*ratio].isIndustrialCenter = coarseGrid[centerPoints[i].x][centerPoints[i].y].isIndustrialCenter;
	}

	ConsolePrinter::printString("Done!");

}

/**
 * Allot the city areas.
 */
void Citygen::allotAreas(std::vector<std::vector<GridElement>> &grid, std::vector<glm::vec2> &centerPoints) {
	ConsolePrinter::printString("Alloting areas...");

	int randSize;
	int magnifyingFactor = FINEGRIDWIDTH / GRIDWIDTH;

	for (int i = 0; i < centerPoints.size(); i++) {
		randSize = rand() % 10000 + 10000;
		if (i == 0) {
			growArea(grid, centerPoints[i].x*magnifyingFactor, centerPoints[i].y*magnifyingFactor, 35000);
		}
		else {
			growArea(grid, centerPoints[i].x*magnifyingFactor, centerPoints[i].y*magnifyingFactor, randSize);
		}

	}

	//Done twice to hopefully remove most errors.
	smoothAreas(grid);
	smoothAreas(grid);

	ConsolePrinter::printString("Done!");
}

/**
 * Grows the areas
 */
void Citygen::growArea(std::vector<std::vector<GridElement>> &grid, int x, int y, int size)
{

	int currentSize = 0;
	std::deque<glm::vec2> landQueue;
	landQueue.push_back(glm::vec2(x, y));
	int nrTendrils = (rand() % 8) + 1;
	glm::vec2 tempPos;
	int chance;
	AreaType areaTypeToGrow;

	if (grid[x][y].isCityCenter)
	{
		areaTypeToGrow = CITYAREA;
	}
	else
	{
		areaTypeToGrow = INDUSTRIALAREA;
	}

	std::vector<std::vector<bool>> highwayVisited(grid.size(), std::vector<bool>(grid.size()));

	/*
	//Makes random area tendrils to make citygen less circular.
	for(int i = 0; i < nrTendrils; i++){
		int tendrilSize = size * 0.1;
		int currentX = x;
		int currentY = y;
		for(int j = 0; j < tendrilSize; j++){
			int xDir = rand()%3 - 1;
			int yDir = rand()%3 - 1;
			int attempts = 0;
			bool failed = false;

			//Get random direction, not super efficient..
			while(((currentX + xDir) < 0) || ((currentX + xDir) >= grid.size()) || ((currentY + yDir) < 0) || ((currentY + yDir) >= grid.size())){
				int xDir = rand()%3 - 1;
				int yDir = rand()%3 - 1;
				attempts++;
				if(attempts == 200){
					failed = true;
					break;
				}
			}
			if(failed){
				break;
			}

			currentX = currentX + xDir;
			currentY = currentY + yDir;
			grid[currentX][currentY].mainAreaType = at;
			landQueue.push_back(glm::vec2(currentX,currentY));
			currentSize++;
		}

	}
	*/

	//Grow city center
	while (currentSize < size && landQueue.size() > 0)
	{
		for (int k = 0; k < landQueue.size(); k++)
		{
			tempPos = landQueue.front();
			landQueue.pop_front();

			for (int i = tempPos.x - 1; i <= tempPos.x + 1; i++)
			{
				for (int j = tempPos.y - 1; j <= tempPos.y + 1; j++)
				{
					if (i != -1 && j != -1 && i != grid.size() && j != grid.size()) {
						if (grid[i][j].mainAreaIsNot(CITYAREA | INDUSTRIALAREA) && !highwayVisited[i][j])
						{
							if (grid[i][j].height > 7.0f || grid[i][j].height < -4.0f)
							{
								chance = 0;
							}
							else if (grid[i][j].height > 5.0f || grid[i][j].height < -2.0f)
							{
								chance = 1;
							}
							else if (grid[i][j].mainAreaIsAny(HIGHWAY | HIGHWAYRESERVED))
							{
								chance = 9;
							}
							else
							{
								if (currentSize < (size / 10))
								{
									chance = 8;
								}
								else
								{
									chance = 4;
								}
							}


							if (rand() % 10 < chance)
							{
								if (grid[i][j].mainAreaIsNot(HIGHWAY | HIGHWAYRESERVED))
								{
									grid[i][j].mainAreaType = areaTypeToGrow;
									currentSize++;
								}
								landQueue.push_back(glm::vec2(i, j));
								highwayVisited[i][j] = true;
							}
						}
					}
				}
			}

			if (currentSize >= size)
			{
				break;
			}
		}
	}

	//Grow residential area
	currentSize = 0;
	while (currentSize < size * 2 && landQueue.size() > 0)
	{
		for (int k = 0; k < landQueue.size(); k++)
		{
			tempPos = landQueue.front();
			landQueue.pop_front();

			for (int i = tempPos.x - 1; i <= tempPos.x + 1; i++)
			{
				for (int j = tempPos.y - 1; j <= tempPos.y + 1; j++)
				{
					if (i != -1 && j != -1 && i != grid.size() && j != grid.size())
					{
						if (grid[i][j].mainAreaIsNot(CITYAREA | RESIDENTIALAREA | INDUSTRIALAREA) && !highwayVisited[i][j])
						{
							if (grid[tempPos.x][tempPos.y].mainAreaIsAny(INDUSTRIALAREA | CITYAREA))
							{
								chance = 1;
							}
							else if (grid[i][j].height > 7.0f || grid[i][j].height < -4.0f)
							{
								chance = 0;
							}
							else if (grid[i][j].height > 5.0f || grid[i][j].height < -2.0f)
							{
								chance = 1;
							}
							else if (grid[i][j].mainAreaIsAny(HIGHWAY | HIGHWAYRESERVED))
							{
								chance = 9;
							}
							else
							{
								chance = 4;
							}


							if (rand() % 10 < chance)
							{
								if (grid[i][j].mainAreaIsNot(HIGHWAY | HIGHWAYRESERVED))
								{
									grid[i][j].mainAreaType = RESIDENTIALAREA;
									currentSize++;
								}
								landQueue.push_back(glm::vec2(i, j));
								highwayVisited[i][j] = true;

							}
						}
					}
				}
			}
			if (currentSize >= size)
			{
				break;
			}
		}
	}
}

/**
 * Fills in small holes made by the growArea algorithm
 */
void Citygen::smoothAreas(std::vector<std::vector<GridElement>> &grid) {

	int gridSize = grid.size();

	for (int i = 0; i < gridSize; i++) {
		for (int j = 0; j < gridSize; j++) {
			if (grid[i][j].mainAreaType == WILDERNESS) {
				int cityCounter = 0;
				int residentialCounter = 0;
				int industrialCounter = 0;
				for (int x = i - 1; x <= i + 1; x++) {
					for (int y = j - 1; y <= j + 1; y++) {
						if (x != -1 && y != -1 && x != grid.size() && y != grid.size()) {
							if (grid[x][y].mainAreaType == CITYAREA) {
								cityCounter++;
							}
							if (grid[x][y].mainAreaType == INDUSTRIALAREA) {
								industrialCounter++;
							}
							if (grid[x][y].mainAreaType == RESIDENTIALAREA) {
								residentialCounter++;
							}

						}
					}
				}
				if ((cityCounter + residentialCounter + industrialCounter) >= 5) {
					if ((cityCounter >= residentialCounter) && (cityCounter >= industrialCounter)) {
						grid[i][j].mainAreaType = CITYAREA;
					}
					else if ((industrialCounter >= residentialCounter) && (industrialCounter >= residentialCounter)) {
						grid[i][j].mainAreaType = INDUSTRIALAREA;
					}
					else {
						grid[i][j].mainAreaType = RESIDENTIALAREA;
					}
				}
			}

		}
	}




}

/**
 *	Takes grid and makes it into triangles that can later be used for parcellation
 */
void Citygen::makeTriangleRepresentation(std::vector<std::vector<GridElement>> &grid)
{
	ConsolePrinter::printString("Making parcels...");

	int gridSize = grid.size();


	for (int i = 0; i < gridSize; i++)
	{
		for (int j = 0; j < gridSize; j++)
		{

			//std::cout << "At:(" << i << "," << j << "), ";
			//std::cout << "Tsq:1, ";
			//std::cout << "I make a new triangle";
			GridElement* currentSquare = &grid[i][j];

			//Create points depending on where in the grid you are and what has already been done.
			currentSquare->midCoord = new glm::vec3(i, grid[i][j].height, j);


			currentSquare->nwEdgeToggle = false;

			currentSquare->neEdgeToggle = false;

			currentSquare->swEdgeToggle = false;

			currentSquare->seEdgeToggle = false;

			//If data to the left and upwards exist..
			if ((i > 0) && (j > 0))
			{
				currentSquare->topLeftCoord = grid[i][j - 1].botLeftCoord;

				currentSquare->topRightCoord = grid[i][j - 1].botRightCoord;

				currentSquare->botLeftCoord = grid[i - 1][j].botRightCoord;

				//Making bot right coord height average
				if ((i == (gridSize - 1)) && (j == (gridSize - 1)))
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, grid[i][j].height, j + 0.5f);
				}
				else if (i == (gridSize - 1))
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i][j + 1].height) / 2, j + 0.5f);
				}
				else if (j == (gridSize - 1))
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height) / 2, j + 0.5f);
				}
				else
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height + grid[i][j + 1].height + grid[i + 1][j + 1].height) / 4, j + 0.5f);
				}

				//Assigning edges
				currentSquare->leftEdgeToggle = grid[i - 1][j].rightEdgeToggle;
				currentSquare->topEdgeToggle = grid[i][j - 1].botEdgeToggle;

				currentSquare->rightEdgeToggle = new bool;
				*(currentSquare->rightEdgeToggle) = false;

				currentSquare->botEdgeToggle = new bool;
				*(currentSquare->botEdgeToggle) = false;

			}
			else if ((i > 0) && (j == 0)) //If data to the left exist..
			{
				currentSquare->topLeftCoord = grid[i - 1][j].topRightCoord;

				currentSquare->botLeftCoord = grid[i - 1][j].botRightCoord;

				//Making top right coord height average
				if (i == (gridSize - 1)) //in top-right corner
				{
					currentSquare->topRightCoord = new glm::vec3(i + 0.5f, grid[i][j].height, j - 0.5f);
				}
				else
				{
					currentSquare->topRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height) / 2, j - 0.5f);
				}

				//Making bot right coord height average
				if ((i == (gridSize - 1)) && (j == (gridSize - 1)))
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, grid[i][j].height, j + 0.5f);
				}
				else if (i == gridSize - 1)
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i][j + 1].height) / 2, j + 0.5f);
				}
				else if (j == gridSize - 1)
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height) / 2, j + 0.5f);
				}
				else
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height + grid[i][j + 1].height + grid[i + 1][j + 1].height) / 4, j + 0.5f);
				}

				//Assigning edges
				currentSquare->leftEdgeToggle = grid[i - 1][j].rightEdgeToggle;

				currentSquare->topEdgeToggle = new bool;
				*(currentSquare->topEdgeToggle) = false;

				currentSquare->rightEdgeToggle = new bool;
				*(currentSquare->rightEdgeToggle) = false;

				currentSquare->botEdgeToggle = new bool;
				*(currentSquare->botEdgeToggle) = false;


			}
			else if ((j > 0) && (i == 0)) //If data upwards exist..
			{
				currentSquare->topLeftCoord = grid[i][j - 1].botLeftCoord;

				currentSquare->topRightCoord = grid[i][j - 1].botRightCoord;

				//Making bot left coord height average
				if (j == (gridSize - 1)) //in bot-left corner
				{
					currentSquare->botLeftCoord = new glm::vec3(i - 0.5f, grid[i][j].height, j + 0.5f);
				}
				else
				{
					currentSquare->botLeftCoord = new glm::vec3(i - 0.5f, (grid[i][j].height + grid[i][j + 1].height) / 2, j + 0.5f);
				}

				//Making bot right coord height average
				if ((i == (gridSize - 1)) && (j == (gridSize - 1)))
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, grid[i][j].height, j + 0.5f);
				}
				else if (i == (gridSize - 1))
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i][j + 1].height) / 2, j + 0.5f);
				}
				else if (j == (gridSize - 1))
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height) / 2, j + 0.5f);
				}
				else
				{
					currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height + grid[i][j + 1].height + grid[i + 1][j + 1].height) / 4, j + 0.5f);
				}

				//Assigning edges
				currentSquare->topEdgeToggle = grid[i][j - 1].botEdgeToggle;

				currentSquare->leftEdgeToggle = new bool;
				*(currentSquare->leftEdgeToggle) = false;

				currentSquare->rightEdgeToggle = new bool;
				*(currentSquare->rightEdgeToggle) = false;

				currentSquare->botEdgeToggle = new bool;
				*(currentSquare->botEdgeToggle) = false;


			}
			else  //No previous data exist.. (Only possible in top-left corner)
			{

				currentSquare->topLeftCoord = new glm::vec3(i - 0.5f, grid[i][j].height, j - 0.5f);

				currentSquare->topRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height) / 2, j - 0.5f);

				currentSquare->botLeftCoord = new glm::vec3(i - 0.5f, (grid[i][j].height + grid[i][j + 1].height) / 2, j + 0.5f);

				currentSquare->botRightCoord = new glm::vec3(i + 0.5f, (grid[i][j].height + grid[i + 1][j].height + grid[i][j + 1].height + grid[i + 1][j + 1].height) / 4, j + 0.5f);

				//Assigning edges
				currentSquare->topEdgeToggle = new bool;
				*(currentSquare->topEdgeToggle) = false;

				currentSquare->leftEdgeToggle = new bool;
				*(currentSquare->leftEdgeToggle) = false;

				currentSquare->rightEdgeToggle = new bool;
				*(currentSquare->rightEdgeToggle) = false;

				currentSquare->botEdgeToggle = new bool;
				*(currentSquare->botEdgeToggle) = false;

			}

			//std::cout << "Made the vertices";

			//Create top triangle-------------------------------
			currentSquare->topTriangleParcelNumber = -1;

			//If road then every triangle in square should also be road
			if (currentSquare->mainAreaIsAny(ROAD | HIGHWAY))
			{
				currentSquare->topTriangleAreaType = currentSquare->mainAreaType;
			}
			//If near road then triangle might be road, highest prio
			else if ((i < (gridSize - 1)) && (j > 1) && (grid[i + 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == HIGHWAY))
			{
				currentSquare->topTriangleAreaType = grid[i][j - 1].mainAreaType;
			}
			else if ((i < (gridSize - 1)) && (j > 1) && (grid[i + 1][j].mainAreaType == ROAD) && (grid[i][j - 1].mainAreaType == ROAD) && !(grid[i + 1][j - 1].mainAreaType == ROAD || grid[i + 1][j - 1].mainAreaType == HIGHWAY))
			{
				currentSquare->topTriangleAreaType = grid[i][j - 1].mainAreaType;
			}
			else if ((i > 0) && (j > 1) && (grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == HIGHWAY)) {
				currentSquare->topTriangleAreaType = grid[i][j - 1].mainAreaType;
			}
			else if ((i > 0) && (j > 1) && (grid[i - 1][j].mainAreaType == ROAD || grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == ROAD || grid[i][j - 1].mainAreaType == HIGHWAY) && !(grid[i - 1][j - 1].mainAreaType == ROAD || grid[i - 1][j - 1].mainAreaType == HIGHWAY))
			{
				currentSquare->topTriangleAreaType = grid[i][j - 1].mainAreaType;
			}
			//If near road reserved
			else if (currentSquare->mainAreaIsAny(WILDERNESS | INDUSTRIALAREA | RESIDENTIALAREA | CITYAREA))
			{
				//If at least one square is roadreserved and the other is either roadreserved or road *exhale*
				if ((i < (gridSize - 1)) && (j > 1) && ((grid[i + 1][j].mainAreaType == ROADRESERVED || grid[i + 1][j].mainAreaType == HIGHWAYRESERVED) || (grid[i][j - 1].mainAreaType == ROADRESERVED || grid[i][j - 1].mainAreaType == HIGHWAYRESERVED)) && (grid[i + 1][j].mainAreaType == ROADRESERVED || grid[i + 1][j].mainAreaType == HIGHWAYRESERVED || grid[i + 1][j].mainAreaType == ROAD || grid[i + 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == ROADRESERVED || grid[i][j - 1].mainAreaType == HIGHWAYRESERVED || grid[i][j - 1].mainAreaType == ROAD || grid[i][j - 1].mainAreaType == HIGHWAY))
				{
					currentSquare->topTriangleAreaType = grid[i][j - 1].mainAreaType;
				}
				else if ((i > 0) && (j > 1) && ((grid[i - 1][j].mainAreaType == ROADRESERVED || grid[i - 1][j].mainAreaType == HIGHWAYRESERVED) || (grid[i][j - 1].mainAreaType == ROADRESERVED || grid[i][j - 1].mainAreaType == HIGHWAYRESERVED)) && (grid[i - 1][j].mainAreaType == ROADRESERVED || grid[i - 1][j].mainAreaType == HIGHWAYRESERVED || grid[i - 1][j].mainAreaType == ROAD || grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == ROADRESERVED || grid[i][j - 1].mainAreaType == HIGHWAYRESERVED || grid[i][j - 1].mainAreaType == ROAD || grid[i][j - 1].mainAreaType == HIGHWAY))
				{
					currentSquare->topTriangleAreaType = grid[i][j - 1].mainAreaType;
				}
				else
				{
					currentSquare->topTriangleAreaType = currentSquare->mainAreaType;
				}

			}
			//Keep it as it is
			else
			{
				currentSquare->topTriangleAreaType = currentSquare->mainAreaType;
			}



			//Create left triangle------------------------------
			currentSquare->leftTriangleParcelNumber = -1;

			//If road then every triangle in square should also be road
			if (currentSquare->mainAreaIsAny(ROAD | HIGHWAY))
			{
				currentSquare->leftTriangleAreaType = currentSquare->mainAreaType;
			}
			//If near road then triangle might be road, highest prio
			else if ((i > 0) && (j < (gridSize - 1)) && (grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j + 1].mainAreaType == HIGHWAY))
			{
				currentSquare->leftTriangleAreaType = grid[i - 1][j].mainAreaType;
			}
			else if ((i > 0) && (j < (gridSize - 1)) && (grid[i - 1][j].mainAreaType == ROAD) && (grid[i][j + 1].mainAreaType == ROAD) && !(grid[i - 1][j + 1].mainAreaType == ROAD || grid[i - 1][j + 1].mainAreaType == HIGHWAY)) {
				currentSquare->leftTriangleAreaType = grid[i - 1][j].mainAreaType;
			}
			else if ((i > 0) && (j > 1) && (grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == HIGHWAY))
			{
				currentSquare->leftTriangleAreaType = grid[i - 1][j].mainAreaType;
			}
			else if ((i > 0) && (j > 1) && (grid[i - 1][j].mainAreaType == ROAD) && (grid[i][j - 1].mainAreaType == ROAD) && !(grid[i - 1][j - 1].mainAreaType == ROAD || grid[i - 1][j - 1].mainAreaType == HIGHWAY))
			{
				currentSquare->leftTriangleAreaType = grid[i - 1][j].mainAreaType;
			}

			//If near road reserved
			else if (currentSquare->mainAreaIsAny(WILDERNESS | INDUSTRIALAREA | RESIDENTIALAREA | CITYAREA))
			{
				if ((i > 0) && (j < (gridSize - 1)) && ((grid[i - 1][j].mainAreaType == ROADRESERVED || grid[i - 1][j].mainAreaType == HIGHWAYRESERVED) || (grid[i][j + 1].mainAreaType == ROADRESERVED || grid[i][j + 1].mainAreaType == HIGHWAYRESERVED)) && (grid[i - 1][j].mainAreaType == ROADRESERVED || grid[i - 1][j].mainAreaType == HIGHWAYRESERVED || grid[i - 1][j].mainAreaType == ROAD || grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j + 1].mainAreaType == ROADRESERVED || grid[i][j + 1].mainAreaType == HIGHWAYRESERVED || grid[i][j + 1].mainAreaType == ROAD || grid[i][j + 1].mainAreaType == HIGHWAY))
				{
					currentSquare->leftTriangleAreaType = grid[i - 1][j].mainAreaType;
				}
				else if ((i > 0) && (j > 1) && ((grid[i - 1][j].mainAreaType == ROADRESERVED || grid[i - 1][j].mainAreaType == HIGHWAYRESERVED) || (grid[i][j - 1].mainAreaType == ROADRESERVED || grid[i][j - 1].mainAreaType == HIGHWAYRESERVED)) && (grid[i - 1][j].mainAreaType == ROADRESERVED || grid[i - 1][j].mainAreaType == HIGHWAYRESERVED || grid[i - 1][j].mainAreaType == ROAD || grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == ROADRESERVED || grid[i][j - 1].mainAreaType == HIGHWAYRESERVED || grid[i][j - 1].mainAreaType == ROAD || grid[i][j - 1].mainAreaType == HIGHWAY))
				{
					currentSquare->leftTriangleAreaType = grid[i - 1][j].mainAreaType;
				}
				else
				{
					currentSquare->leftTriangleAreaType = currentSquare->mainAreaType;
				}
			}

			//Keep it as it is
			else
			{
				currentSquare->leftTriangleAreaType = currentSquare->mainAreaType;
			}


			//Create right triangle------------------------------
			currentSquare->rightTriangleParcelNumber = -1;

			//If road then every triangle in square should also be road
			if (currentSquare->mainAreaIsAny(ROAD | HIGHWAY))
			{
				currentSquare->rightTriangleAreaType = currentSquare->mainAreaType;
			}
			//If near road then triangle might be road, highest prio
			else if ((i < (gridSize - 1)) && (j > 1) && (grid[i + 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == HIGHWAY))
			{
				currentSquare->rightTriangleAreaType = grid[i + 1][j].mainAreaType;
			}
			else if ((i < (gridSize - 1)) && (j > 1) && (grid[i + 1][j].mainAreaType == ROAD) && (grid[i][j - 1].mainAreaType == ROAD) && !(grid[i + 1][j - 1].mainAreaType == ROAD || grid[i + 1][j - 1].mainAreaType == HIGHWAY))
			{
				currentSquare->rightTriangleAreaType = grid[i + 1][j].mainAreaType;
			}
			else if ((i < (gridSize - 1)) && (j < (gridSize - 1)) && (grid[i + 1][j].mainAreaType == HIGHWAY) && (grid[i][j + 1].mainAreaType == HIGHWAY))
			{
				currentSquare->rightTriangleAreaType = grid[i + 1][j].mainAreaType;
			}
			else if ((i < (gridSize - 1)) && (j < (gridSize - 1)) && (grid[i + 1][j].mainAreaType == ROAD) && (grid[i][j + 1].mainAreaType == ROAD) && !(grid[i + 1][j + 1].mainAreaType == ROAD || grid[i + 1][j + 1].mainAreaType == HIGHWAY))
			{
				currentSquare->rightTriangleAreaType = grid[i + 1][j].mainAreaType;
			}
			//If near road reserved
			else if (currentSquare->mainAreaIsAny(WILDERNESS | INDUSTRIALAREA | RESIDENTIALAREA | CITYAREA))
			{
				if ((i < (gridSize - 1)) && (j > 1) && ((grid[i + 1][j].mainAreaType == ROADRESERVED || grid[i + 1][j].mainAreaType == HIGHWAYRESERVED) || (grid[i][j - 1].mainAreaType == ROADRESERVED || grid[i][j - 1].mainAreaType == HIGHWAYRESERVED)) && (grid[i + 1][j].mainAreaType == ROADRESERVED || grid[i + 1][j].mainAreaType == HIGHWAYRESERVED || grid[i + 1][j].mainAreaType == ROAD || grid[i + 1][j].mainAreaType == HIGHWAY) && (grid[i][j - 1].mainAreaType == ROADRESERVED || grid[i][j - 1].mainAreaType == HIGHWAYRESERVED || grid[i][j - 1].mainAreaType == ROAD || grid[i][j - 1].mainAreaType == HIGHWAY))
				{
					currentSquare->rightTriangleAreaType = grid[i + 1][j].mainAreaType;
				}
				else if ((i < (gridSize - 1)) && (j < (gridSize - 1)) && ((grid[i + 1][j].mainAreaType == ROADRESERVED || grid[i + 1][j].mainAreaType == HIGHWAYRESERVED) || (grid[i][j + 1].mainAreaType == ROADRESERVED || grid[i][j + 1].mainAreaType == HIGHWAYRESERVED)) && (grid[i + 1][j].mainAreaType == ROADRESERVED || grid[i + 1][j].mainAreaType == HIGHWAYRESERVED || grid[i + 1][j].mainAreaType == ROAD || grid[i + 1][j].mainAreaType == HIGHWAY) && (grid[i][j + 1].mainAreaType == ROADRESERVED || grid[i][j + 1].mainAreaType == HIGHWAYRESERVED || grid[i][j + 1].mainAreaType == ROAD || grid[i][j + 1].mainAreaType == HIGHWAY))
				{
					currentSquare->rightTriangleAreaType = grid[i + 1][j].mainAreaType;
				}
				else {
					currentSquare->rightTriangleAreaType = currentSquare->mainAreaType;
				}
			}
			//Keep it as it is
			else {
				currentSquare->rightTriangleAreaType = currentSquare->mainAreaType;
			}




			//Create bottom triangle-------------------------------
			currentSquare->botTriangleParcelNumber = -1;

			//If road then every triangle in square should also be road
			if (currentSquare->mainAreaIsAny(ROAD | HIGHWAY))
			{
				currentSquare->botTriangleAreaType = currentSquare->mainAreaType;
			}
			//If near road then triangle might be road, highest prio
			else if ((i < (gridSize - 1)) && (j < (gridSize - 1)) && (grid[i + 1][j].mainAreaType == HIGHWAY) && (grid[i][j + 1].mainAreaType == HIGHWAY))
			{
				currentSquare->botTriangleAreaType = grid[i][j + 1].mainAreaType;
			}
			else if ((i < (gridSize - 1)) && (j < (gridSize - 1)) && (grid[i + 1][j].mainAreaType == ROAD) && (grid[i][j + 1].mainAreaType == ROAD) && !(grid[i + 1][j + 1].mainAreaType == ROAD || grid[i + 1][j + 1].mainAreaType == HIGHWAY))
			{
				currentSquare->botTriangleAreaType = grid[i][j + 1].mainAreaType;
			}
			else if ((i > 1) && (j < (gridSize - 1)) && (grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j + 1].mainAreaType == HIGHWAY))
			{
				currentSquare->botTriangleAreaType = grid[i][j + 1].mainAreaType;
			}
			else if ((i > 1) && (j < (gridSize - 1)) && (grid[i - 1][j].mainAreaType == ROAD) && (grid[i][j + 1].mainAreaType == ROAD) && !(grid[i - 1][j + 1].mainAreaType == ROAD || grid[i - 1][j + 1].mainAreaType == HIGHWAY))
			{
				currentSquare->botTriangleAreaType = grid[i][j + 1].mainAreaType;
			}
			//If near road reserved
			else if (currentSquare->mainAreaIsAny(WILDERNESS | INDUSTRIALAREA | RESIDENTIALAREA | CITYAREA))
			{
				if ((i < (gridSize - 1)) && (j < (gridSize - 1)) && ((grid[i + 1][j].mainAreaType == ROADRESERVED || grid[i + 1][j].mainAreaType == HIGHWAYRESERVED) || (grid[i][j + 1].mainAreaType == ROADRESERVED || grid[i][j + 1].mainAreaType == HIGHWAYRESERVED)) && (grid[i + 1][j].mainAreaType == ROADRESERVED || grid[i + 1][j].mainAreaType == HIGHWAYRESERVED || grid[i + 1][j].mainAreaType == ROAD || grid[i + 1][j].mainAreaType == HIGHWAY) && (grid[i][j + 1].mainAreaType == ROADRESERVED || grid[i][j + 1].mainAreaType == HIGHWAYRESERVED || grid[i][j + 1].mainAreaType == ROAD || grid[i][j + 1].mainAreaType == HIGHWAY))
				{
					currentSquare->botTriangleAreaType = grid[i][j + 1].mainAreaType;
				}
				else if ((i > 1) && (j < (gridSize - 1)) && ((grid[i - 1][j].mainAreaType == ROADRESERVED || grid[i - 1][j].mainAreaType == HIGHWAYRESERVED) || (grid[i][j + 1].mainAreaType == ROADRESERVED || grid[i][j + 1].mainAreaType == HIGHWAYRESERVED)) && (grid[i - 1][j].mainAreaType == ROADRESERVED || grid[i - 1][j].mainAreaType == HIGHWAYRESERVED || grid[i - 1][j].mainAreaType == ROAD || grid[i - 1][j].mainAreaType == HIGHWAY) && (grid[i][j + 1].mainAreaType == ROADRESERVED || grid[i][j + 1].mainAreaType == HIGHWAYRESERVED || grid[i][j + 1].mainAreaType == ROAD || grid[i][j + 1].mainAreaType == HIGHWAY))
				{
					currentSquare->botTriangleAreaType = grid[i][j + 1].mainAreaType;
				}
				else
				{
					currentSquare->botTriangleAreaType = currentSquare->mainAreaType;
				}
			}
			//Keep it as it is
			else
			{
				currentSquare->botTriangleAreaType = currentSquare->mainAreaType;
			}
		}
	}
	ConsolePrinter::printString("Done!");
}

/**
 * Makes "bridges" from roads and lower the terrain on lakes to minimize visual water glitches
 */
void Citygen::waterFix(std::vector<std::vector<GridElement>> &grid) {
	for (int i = 0; i < grid.size(); i++) {
		for (int j = 0; j < grid.size(); j++) {
			//Fix depth
			if (grid[i][j].topTriangleAreaType == WILDERNESS) {
				if (grid[i][j].topLeftCoord->y <= -4.0f) {
					grid[i][j].topLeftCoord->y = -4.3f;
				}
				if (grid[i][j].topRightCoord->y <= -4.0f) {
					grid[i][j].topRightCoord->y = -4.3f;
				}
				if (grid[i][j].midCoord->y <= -4.0f) {
					grid[i][j].midCoord->y = -4.3f;
				}
			}
			if (grid[i][j].leftTriangleAreaType == WILDERNESS) {
				if (grid[i][j].topLeftCoord->y <= -4.0f) {
					grid[i][j].topLeftCoord->y = -4.3f;
				}
				if (grid[i][j].botLeftCoord->y <= -4.0f) {
					grid[i][j].botLeftCoord->y = -4.3f;
				}
				if (grid[i][j].midCoord->y <= -4.0f) {
					grid[i][j].midCoord->y = -4.3f;
				}
			}
			if (grid[i][j].rightTriangleAreaType == WILDERNESS) {
				if (grid[i][j].topRightCoord->y <= -4.0f) {
					grid[i][j].topRightCoord->y = -4.3f;
				}
				if (grid[i][j].botRightCoord->y <= -4.0f) {
					grid[i][j].botRightCoord->y = -4.3f;
				}
				if (grid[i][j].midCoord->y <= -4.0f) {
					grid[i][j].midCoord->y = -4.3f;
				}
			}
			if (grid[i][j].botTriangleAreaType == WILDERNESS) {
				if (grid[i][j].botLeftCoord->y <= -4.0f) {
					grid[i][j].botLeftCoord->y = -4.3f;
				}
				if (grid[i][j].botRightCoord->y <= -4.0f) {
					grid[i][j].botRightCoord->y = -4.3f;
				}
				if (grid[i][j].midCoord->y <= -4.0f) {
					grid[i][j].midCoord->y = -4.3f;
				}
			}

			//Fix bridges
			if (grid[i][j].topTriangleAreaType == ROAD || grid[i][j].topTriangleAreaType == HIGHWAY || grid[i][j].topTriangleAreaType == ROADRESERVED || grid[i][j].topTriangleAreaType == HIGHWAYRESERVED) {
				if (grid[i][j].topLeftCoord->y <= -4.0f) {
					grid[i][j].topLeftCoord->y = -3.99f;
				}
				if (grid[i][j].topRightCoord->y <= -4.0f) {
					grid[i][j].topRightCoord->y = -3.99f;
				}
				if (grid[i][j].midCoord->y <= -4.0f) {
					grid[i][j].midCoord->y = -3.99f;
				}
			}
			if (grid[i][j].leftTriangleAreaType == ROAD || grid[i][j].leftTriangleAreaType == HIGHWAY || grid[i][j].leftTriangleAreaType == ROADRESERVED || grid[i][j].leftTriangleAreaType == HIGHWAYRESERVED) {
				if (grid[i][j].topLeftCoord->y <= -4.0f) {
					grid[i][j].topLeftCoord->y = -3.99f;
				}
				if (grid[i][j].botLeftCoord->y <= -4.0f) {
					grid[i][j].botLeftCoord->y = -3.99f;
				}
				if (grid[i][j].midCoord->y <= -4.0f) {
					grid[i][j].midCoord->y = -3.99f;
				}
			}
			if (grid[i][j].rightTriangleAreaType == ROAD || grid[i][j].rightTriangleAreaType == HIGHWAY || grid[i][j].rightTriangleAreaType == ROADRESERVED || grid[i][j].rightTriangleAreaType == HIGHWAYRESERVED) {
				if (grid[i][j].topRightCoord->y <= -4.0f) {
					grid[i][j].topRightCoord->y = -3.99f;
				}
				if (grid[i][j].botRightCoord->y <= -4.0f) {
					grid[i][j].botRightCoord->y = -3.99f;
				}
				if (grid[i][j].midCoord->y <= -4.0f) {
					grid[i][j].midCoord->y = -3.99f;
				}
			}
			if (grid[i][j].botTriangleAreaType == ROAD || grid[i][j].botTriangleAreaType == HIGHWAY || grid[i][j].botTriangleAreaType == ROADRESERVED || grid[i][j].botTriangleAreaType == HIGHWAYRESERVED) {
				if (grid[i][j].botLeftCoord->y <= -4.0f) {
					grid[i][j].botLeftCoord->y = -3.99f;
				}
				if (grid[i][j].botRightCoord->y <= -4.0f) {
					grid[i][j].botRightCoord->y = -3.99f;
				}
				if (grid[i][j].midCoord->y <= -4.0f) {
					grid[i][j].midCoord->y = -3.99f;
				}
			}





		}
	}
}


/**
 * Makes grid into plots
 */
void Citygen::makePlots(std::vector<std::vector<GridElement>> &grid, std::vector<Plot> &plots)
{
	ConsolePrinter::printString("Plotting...");

	//Make parcels
	int regionCounter = 0;
	for (int i = 0; i < grid.size(); i++)
	{
		for (int j = 0; j < grid.size(); j++)
		{
			if (!(grid[i][j].isChecked))
			{
				if (grid[i][j].mainAreaIsNot(WILDERNESS | ROAD | HIGHWAY | ROADRESERVED | HIGHWAYRESERVED))
				{
					floodFillHouseParcel(grid, i, j, regionCounter);
					regionCounter++;
				}
			}

		}
	}

	std::deque<std::vector<GridElement*>> parcelVector(regionCounter);
	for (int i = 0; i < grid.size(); i++)
	{
		for (int j = 0; j < grid.size(); j++)
		{
			if (grid[i][j].mainAreaIsNot(WILDERNESS | ROAD | HIGHWAY | ROADRESERVED | HIGHWAYRESERVED))
			{
				if (grid[i][j].topTriangleParcelNumber != -1)
				{
					parcelVector[grid[i][j].topTriangleParcelNumber].push_back(&grid[i][j]);
					continue;
				}
				if (grid[i][j].botTriangleParcelNumber != -1)
				{
					parcelVector[grid[i][j].botTriangleParcelNumber].push_back(&grid[i][j]);
					continue;
				}
				if (grid[i][j].leftTriangleParcelNumber != -1)
				{
					parcelVector[grid[i][j].leftTriangleParcelNumber].push_back(&grid[i][j]);
					continue;
				}
				if (grid[i][j].rightTriangleParcelNumber != -1)
				{
					parcelVector[grid[i][j].rightTriangleParcelNumber].push_back(&grid[i][j]);
					continue;
				}
			}
		}
	}

	for (int i = 0; i < parcelVector.size(); i++)
	{
		std::vector<GridElement*> parcel = parcelVector[i];
		//Toggle edges for wall-finding
		for (int parcelElement = 0; parcelElement < parcel.size(); parcelElement++)
		{

			if (parcel[parcelElement]->topTriangleParcelNumber != -1)
			{
				//Top triangle

				//Toggle edges
				*(parcel[parcelElement]->topEdgeToggle) = !(*(parcel[parcelElement]->topEdgeToggle));
				parcel[parcelElement]->neEdgeToggle = !(parcel[parcelElement]->neEdgeToggle);
				parcel[parcelElement]->nwEdgeToggle = !(parcel[parcelElement]->nwEdgeToggle);


			}


			if (parcel[parcelElement]->leftTriangleParcelNumber != -1)
			{
				//Left triangle

				//toggle edges
				*(parcel[parcelElement]->leftEdgeToggle) = !(*(parcel[parcelElement]->leftEdgeToggle));
				parcel[parcelElement]->swEdgeToggle = !(parcel[parcelElement]->swEdgeToggle);
				parcel[parcelElement]->nwEdgeToggle = !(parcel[parcelElement]->nwEdgeToggle);

			}


			if (parcel[parcelElement]->rightTriangleParcelNumber != -1)
			{
				//Right triangle

				//toggle edges
				*(parcel[parcelElement]->rightEdgeToggle) = !(*(parcel[parcelElement]->rightEdgeToggle));
				parcel[parcelElement]->seEdgeToggle = !(parcel[parcelElement]->seEdgeToggle);
				parcel[parcelElement]->neEdgeToggle = !(parcel[parcelElement]->neEdgeToggle);

			}

			if (parcel[parcelElement]->botTriangleParcelNumber != -1)
			{
				//Bot triangle


				//toggle edges
				*(parcel[parcelElement]->botEdgeToggle) = !(*(parcel[parcelElement]->botEdgeToggle));
				parcel[parcelElement]->swEdgeToggle = !(parcel[parcelElement]->swEdgeToggle);
				parcel[parcelElement]->seEdgeToggle = !(parcel[parcelElement]->seEdgeToggle);

			}

		}

	}

	//std::cout << "Toggled the parcels\n";
	for (int i = 0; i < parcelVector.size(); i++)
	{

		std::vector<GridElement*> parcel = parcelVector[i];
		if (parcel.size() < 10)
		{
			continue;
		}
		int noOfDivisions = rand() % 10;
		std::list<std::vector<GridElement*>> plotList;
		plotList.push_back(parcel);

		//std::cout << "Initialized things\n";

		for (int j = 0; j < 1; j++)
		{
			std::list<std::vector<GridElement*>>::iterator it = plotList.begin();
			int advancement = (rand() % plotList.size() - 1);
			if (advancement > 0)
			{
				std::advance(it, advancement);
			}

			std::vector<GridElement*> oldPlot = *it;
			std::vector<GridElement*> firstPlot;
			std::vector<GridElement*> secondPlot;

			std::vector<GridElement*> wallElements;
			std::vector<GridElement*> midElements;

			//std::cout << "Made IT\n";
			for (int k = 0; k < oldPlot.size(); k++) {
				if (*(oldPlot[k]->botEdgeToggle) || *(oldPlot[k]->topEdgeToggle) || *(oldPlot[k]->leftEdgeToggle) || *(oldPlot[k]->rightEdgeToggle) ||
					oldPlot[k]->seEdgeToggle || oldPlot[k]->swEdgeToggle || oldPlot[k]->neEdgeToggle || oldPlot[k]->nwEdgeToggle)
				{
					wallElements.push_back(oldPlot[k]);
				}
				else
				{
					midElements.push_back(oldPlot[k]);
				}
			}

			//std::cout << "Divided into walls and mid\n";

			if (midElements.size() < 9)
			{
				continue;
			}

			GridElement* randomWallElement = wallElements[rand() % wallElements.size()];
			while (true)
			{
				int wallCounter = 0;
				if (*(randomWallElement->botEdgeToggle))
				{
					wallCounter++;
				}
				if (*(randomWallElement->topEdgeToggle))
				{
					wallCounter++;
				}
				if (*(randomWallElement->leftEdgeToggle))
				{
					wallCounter++;
				}
				if (*(randomWallElement->rightEdgeToggle))
				{
					wallCounter++;
				}
				if (randomWallElement->neEdgeToggle)
				{
					wallCounter++;
				}
				if (randomWallElement->seEdgeToggle)
				{
					wallCounter++;
				}
				if (randomWallElement->nwEdgeToggle)
				{
					wallCounter++;
				}
				if (randomWallElement->swEdgeToggle)
				{
					wallCounter++;
				}
				if (wallCounter == 1)
				{
					break;
				}
				else if (wallCounter == 2)
				{
					if ((randomWallElement->neEdgeToggle && randomWallElement->swEdgeToggle) || (randomWallElement->nwEdgeToggle && randomWallElement->seEdgeToggle))
					{
						break;
					}
				}

				randomWallElement = wallElements[rand() % wallElements.size()];
			}

			//std::cout << "Counted walls\n";

			//New region
			int newRegion = ++regionCounter;


			//Guaranteed to be the parcelnumber of the current parcel, since no triangle can be outside the parcel.
			int currentRegion = midElements[0]->botTriangleParcelNumber;

			bool divided = false;
			bool diag = false;
			//Cut along Y axis..
			if (*(randomWallElement->botEdgeToggle) || *(randomWallElement->topEdgeToggle))
			{
				divided = true;
				int xCutoff = randomWallElement->midCoord->x;
				for (int k = 0; k < oldPlot.size(); k++)
				{
					if (oldPlot[k]->midCoord->x == xCutoff)
					{
						if (oldPlot[k]->rightTriangleParcelNumber == currentRegion)
						{
							*(oldPlot[k]->rightEdgeToggle) = true;
						}
						firstPlot.push_back(oldPlot[k]);
					}
					else if (oldPlot[k]->midCoord->x < xCutoff)
					{
						firstPlot.push_back(oldPlot[k]);
					}
					else
					{
						secondPlot.push_back(oldPlot[k]);
						if (oldPlot[k]->botTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->botTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->topTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->topTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->leftTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->leftTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->rightTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->rightTriangleParcelNumber = newRegion;
						}

					}
				}
			}

			//Cut along X axis..
			else if (*(randomWallElement->leftEdgeToggle) || *(randomWallElement->rightEdgeToggle))
			{
				divided = true;
				int yCutoff = randomWallElement->midCoord->z; //y in 3d space is height..
				for (int k = 0; k < oldPlot.size(); k++)
				{
					if (oldPlot[k]->midCoord->z == yCutoff)
					{
						if (oldPlot[k]->botTriangleParcelNumber == currentRegion)
						{
							*(oldPlot[k]->botEdgeToggle) = true;
						}
						firstPlot.push_back(oldPlot[k]);
					}
					else if (oldPlot[k]->midCoord->z < yCutoff)
					{
						firstPlot.push_back(oldPlot[k]);
					}
					else
					{
						secondPlot.push_back(oldPlot[k]);
						if (oldPlot[k]->botTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->botTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->topTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->topTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->leftTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->leftTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->rightTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->rightTriangleParcelNumber = newRegion;
						}
					}
				}
			}

			//Cut diagonally NW to SE TODO:vvv FIX THIS vvv Try toggle fix..
			else if ((randomWallElement->neEdgeToggle && randomWallElement->swEdgeToggle))
			{
				divided = true;

				int p0x = randomWallElement->midCoord->x;
				int p0y = randomWallElement->midCoord->z;
				int p1x = p0x - 1;
				int p1y = p0y - 1;

				for (int k = 0; k < oldPlot.size(); k++)
				{
					float dist = ((p0y - p1y)*oldPlot[k]->midCoord->x + (p1x - p0x)*oldPlot[k]->midCoord->z + (p0x*p1y - p1x * p0y)) / (std::sqrt((float)((p1x - p0x)*(p1x - p0x) + (p1y - p0y)*(p1y - p0y))));
					if (dist < 0.1f && dist > -0.1f)
					{
						firstPlot.push_back(oldPlot[k]);
						secondPlot.push_back(oldPlot[k]);

						if (oldPlot[k]->botTriangleParcelNumber == currentRegion && oldPlot[k]->rightTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->seEdgeToggle = true;
						}
						if (oldPlot[k]->topTriangleParcelNumber == currentRegion && oldPlot[k]->leftTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->nwEdgeToggle = true;
						}
						float cornerDist = ((p0y - p1y)*oldPlot[k]->topRightCoord->x + (p1x - p0x)*oldPlot[k]->topRightCoord->z + (p0x*p1y - p1x * p0y)) / (std::sqrt((float)((p1x - p0x)*(p1x - p0x) + (p1y - p0y)*(p1y - p0y))));
						if (cornerDist < 0)
						{
							if (oldPlot[k]->leftTriangleParcelNumber != -1)
							{
								oldPlot[k]->leftTriangleParcelNumber = newRegion;
							}
							if (oldPlot[k]->botTriangleParcelNumber != -1)
							{
								oldPlot[k]->botTriangleParcelNumber = newRegion;
							}
						}
						else
						{
							if (oldPlot[k]->topTriangleParcelNumber != -1)
							{
								oldPlot[k]->topTriangleParcelNumber = newRegion;
							}
							if (oldPlot[k]->rightTriangleParcelNumber != -1)
							{
								oldPlot[k]->rightTriangleParcelNumber = newRegion;
							}
						}
					}
					else if (dist < -0.1f)
					{
						firstPlot.push_back(oldPlot[k]);
					}
					else
					{
						secondPlot.push_back(oldPlot[k]);
						if (oldPlot[k]->botTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->botTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->topTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->topTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->leftTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->leftTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->rightTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->rightTriangleParcelNumber = newRegion;
						}
					}

				}



			}

			//Cut diagonally NE to SW
			else if ((randomWallElement->nwEdgeToggle && randomWallElement->seEdgeToggle))
 {
				divided = true;
				int p0x = randomWallElement->midCoord->x;
				int p0y = randomWallElement->midCoord->z;
				int p1x = p0x + 1;
				int p1y = p0y - 1;

				for (int k = 0; k < oldPlot.size(); k++)
				{
					float dist = ((p0y - p1y)*oldPlot[k]->midCoord->x + (p1x - p0x)*oldPlot[k]->midCoord->z + (p0x*p1y - p1x * p0y)) / (std::sqrt((float)((p1x - p0x)*(p1x - p0x) + (p1y - p0y)*(p1y - p0y))));
					if (dist < 0.1f && dist > -0.1f)
					{
						firstPlot.push_back(oldPlot[k]);
						secondPlot.push_back(oldPlot[k]);

						if (oldPlot[k]->botTriangleParcelNumber != -1 && oldPlot[k]->leftTriangleParcelNumber != -1)
						{
							oldPlot[k]->swEdgeToggle = true;
						}
						if (oldPlot[k]->topTriangleParcelNumber != -1 && oldPlot[k]->rightTriangleParcelNumber != -1)
						{
							oldPlot[k]->neEdgeToggle = true;
						}
						float cornerDist = ((p0y - p1y)*oldPlot[k]->topLeftCoord->x + (p1x - p0x)*oldPlot[k]->topLeftCoord->z + (p0x*p1y - p1x * p0y)) / (std::sqrt((float)((p1x - p0x)*(p1x - p0x) + (p1y - p0y)*(p1y - p0y))));
						if (cornerDist < 0)
						{
							if (oldPlot[k]->rightTriangleParcelNumber != -1)
							{
								oldPlot[k]->rightTriangleParcelNumber = newRegion;
							}
							if (oldPlot[k]->botTriangleParcelNumber != -1)
							{
								oldPlot[k]->botTriangleParcelNumber = newRegion;
							}
						}
						else
						{
							if (oldPlot[k]->topTriangleParcelNumber != -1)
							{
								oldPlot[k]->topTriangleParcelNumber = newRegion;
							}
							if (oldPlot[k]->leftTriangleParcelNumber != -1)
							{
								oldPlot[k]->leftTriangleParcelNumber = newRegion;
							}
						}
					}
					else if (dist < -0.1)
					{
						firstPlot.push_back(oldPlot[k]);
					}
					else
					{
						secondPlot.push_back(oldPlot[k]);
						if (oldPlot[k]->botTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->botTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->topTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->topTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->leftTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->leftTriangleParcelNumber = newRegion;
						}
						if (oldPlot[k]->rightTriangleParcelNumber == currentRegion)
						{
							oldPlot[k]->rightTriangleParcelNumber = newRegion;
						}
					}

				}
			}

			//std::cout << "Made it bfore erase\n";
			if (divided)
			{
				plotList.erase(it); //the iterator that holds the old plot, if that was unclear...
				plotList.push_back(firstPlot);
				plotList.push_back(secondPlot);
			}
			//std::cout << "Made it after erase\n";
		} //End of divisions
		//plots.push_back(Plot(HOUSE, *(plotList.begin())));

		for (std::list<std::vector<GridElement*>>::iterator it = plotList.begin(); it != plotList.end(); ++it)
		{
			for (int k = 0; k < (*it).size(); k++)
			{
				if ((*it)[k]->isNextToRoad)
				{
					plots.push_back(Plot(HOUSE, *it));
					break;
				}
			}

		}
		//std::cout << "Made it after adding plots\n";
	}//End of loop through parcels
	//std::cout << "Made it through all plots\n";
	ConsolePrinter::printString("Done!");
}

