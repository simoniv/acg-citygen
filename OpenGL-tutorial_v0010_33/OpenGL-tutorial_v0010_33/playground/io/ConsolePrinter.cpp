#include "ConsolePrinter.h"

std::string ConsolePrinter::previousProgressString = "";
int ConsolePrinter::previousProgressNumber = -1;

void ConsolePrinter::printString(const std::string stringToPrint)
{
	std::cout << stringToPrint << std::endl;
	previousProgressString = "";
}

void ConsolePrinter::printProgress(const std::string stringToPrint, const int progress)
{
	if (previousProgressNumber == progress)
	{
		return;
	}

	if (stringToPrint == previousProgressString)
	{
		std::cout << "\r" << stringToPrint << std::to_string(progress) << "% " << std::flush;
	}
	else
	{
		std::cout << stringToPrint << std::to_string(progress) << "%" << std::flush;
	}

	previousProgressString = stringToPrint;
	previousProgressNumber = progress;
}

void ConsolePrinter::progressFinished()
{
	std::cout << std::endl;
	previousProgressString = "";
	previousProgressNumber = -1;
}