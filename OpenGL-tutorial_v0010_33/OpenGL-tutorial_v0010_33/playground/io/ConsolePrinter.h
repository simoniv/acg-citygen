#pragma once

#include <iostream>
#include <string>

/**
 * Class for printing stuff to console
 *
 */
class ConsolePrinter
{
public:
	static void printString(const std::string stringToPrint);
	static void printProgress(const std::string stringToPrint, const int progress);
	static void progressFinished();

private:
	static std::string previousProgressString;
	static int previousProgressNumber;

};