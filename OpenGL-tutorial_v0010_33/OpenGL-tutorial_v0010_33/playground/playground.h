#include <stdio.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <list>
#include <algorithm>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>


#include <GL/glew.h>

#include <GL/glfw.h>

#include <common/shader.hpp>
#include <common/controls.hpp>

#include <math.h>
#include <time.h>

#include "generation/GridElement.h"
#include "generation/Plot.h"


class Citygen
{
public:
	Citygen() {}
	~Citygen() {}

	//Forward referencing
	int run();
	int setupWindow();
	void generateTerrain(std::vector<std::vector<GridElement>> &grid);
	void generateCenters(std::vector<std::vector<GridElement>> &grid, std::vector<glm::vec2> &centerPoints, int noOfPoints);
	void generateHighway(std::vector<std::vector<GridElement>> &grid, std::vector<glm::vec2> &cityCenters, std::deque<std::vector<glm::vec2>> &road);
	void generateMainRoads(std::vector<std::vector<GridElement>> &grid, std::vector<std::vector<GridElement>> &coarsegrid, std::vector<glm::vec2> &centerPoints, std::deque<std::vector<glm::vec2>> &road);
	void generateMinorRoads(std::vector<std::vector<GridElement>> &grid);
	void reserveRoadMargin(std::vector<std::vector<GridElement>> &grid, int x, int y, int size, AreaType type);
	void bezierGrid(std::vector<std::vector<GridElement>> & fineGrid, std::deque<std::vector<glm::vec2>>  &road, int roadReserveSize, AreaType type);
	void floodFillArea(std::vector<std::vector<GridElement>> &grid, int x, int y, int regionCounter);
	void interpolateGrid(std::vector<std::vector<GridElement>> &fineGrid, std::vector<std::vector<GridElement>> &coarseGrid, std::vector<glm::vec2> &centerPoints);
	void allotAreas(std::vector<std::vector<GridElement>> &grid, std::vector<glm::vec2> &centerPoints);
	void growArea(std::vector<std::vector<GridElement>> &grid, int x, int y, int size);
	void smoothAreas(std::vector<std::vector<GridElement>> &grid);
	void waterFix(std::vector<std::vector<GridElement>> &grid);
	void makeTriangleRepresentation(std::vector<std::vector<GridElement>> &grid);
	void makePlots(std::vector<std::vector<GridElement>> &grid, std::vector<Plot> &plots);
	void floodFillHouseParcel(std::vector<std::vector<GridElement>> &grid, int x, int y, int regionCounter);

};