#version 330 core

in vec3 fragmentColor;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;
in vec3 SunDirection_cameraspace;
in vec2 UV;

out vec3 color;

uniform sampler2D textureSampler;
uniform sampler2D lightMapSampler;

uniform mat4 MV;
uniform vec3 LightPosition_worldspace;

uniform vec3 LightColor;
uniform float LightPower;

uniform int hasTexture;

uniform float timeOfDay;

void main(){


	// Normal of the computed fragment, in camera space
	vec3 n = normalize( Normal_cameraspace );
	// Direction of the light (from the fragment to the light)
	vec3 l = normalize( LightDirection_cameraspace );
	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float cosTheta = clamp( dot( n,l ), 0,1 );

	

    //color = fragmentColor;
	// Material properties
	vec3 MaterialDiffuseColor = fragmentColor;
	vec3 MaterialAmbientColor;
	if(hasTexture == 0){
		MaterialAmbientColor = vec3(0.1,0.1,0.1) * MaterialDiffuseColor;
	} else { //Dirty lighting fix during midday
		float middayAddon = clamp(timeOfDay-0.7-cosTheta,0,1);
		MaterialAmbientColor = vec3(0.1+middayAddon,0.1+middayAddon,0.1+middayAddon) * texture( textureSampler, UV ).rgb;
	}

	vec3 lightMapSample = texture( lightMapSampler, UV ).rgb;
	vec3 MaterialSpecularColor;
	if(lightMapSample.x == 0.0 && lightMapSample.y == 0.0 && lightMapSample.z == 0.0){
		MaterialSpecularColor = vec3(0.0,0.0,0.0);
	} else {
		MaterialSpecularColor = vec3(0.9,0.9,0.9);
	}

	// Distance to the light
	float distance = length( LightPosition_worldspace - Position_worldspace );


	
	// Eye vector (towards the camera)
	vec3 E = normalize(EyeDirection_cameraspace);
	// Direction in which the triangle reflects the light
	vec3 sunl = normalize(SunDirection_cameraspace);
	vec3 R = reflect(-l,n);
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float cosAlpha = pow(clamp( dot( E,R ), 0,1 ), 15);
	
	if(hasTexture == 0){
		color = 
			// Ambient : simulates indirect lighting
			MaterialAmbientColor +
			// Diffuse : "color" of the object
			MaterialDiffuseColor * LightColor * LightPower * cosTheta;
	} else if(lightMapSample.x == 0.0 && lightMapSample.y == 0.0 && lightMapSample.z == 0.0){
		color = MaterialDiffuseColor * LightColor * LightPower * cosTheta + MaterialAmbientColor + 
		texture( lightMapSampler, UV ).rgb * clamp((0.5 - (timeOfDay)),0,1) + MaterialSpecularColor*cosAlpha*clamp(timeOfDay, 0, 1)*2;
	} else {
		color = texture( textureSampler, UV ).rgb * LightColor * LightPower * cosTheta + MaterialAmbientColor + 
		texture( lightMapSampler, UV ).rgb * clamp((0.5 - (timeOfDay)),0,1) + MaterialSpecularColor*cosAlpha*clamp(timeOfDay, 0, 1)*2;
	}


}