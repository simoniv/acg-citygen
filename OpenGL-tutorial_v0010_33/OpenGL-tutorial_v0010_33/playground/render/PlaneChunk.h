#pragma once

#include <vector>

#include "Chunk.h"
#include "../generation/GridElement.h"

class PlaneChunk : public Chunk
{
public:
	PlaneChunk(std::vector<std::vector<GridElement>> &grid, int chunkNr, int chunkSize, int chunkRes);
};