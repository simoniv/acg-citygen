#pragma once

#include <GL/glew.h>

#include "Chunk.h"
#include "../generation/Plot.h"

//House color arrays TODO: Change to std::vector
const glm::vec3 skyscraperColors[] = {
	glm::vec3(0.48235f, 0.47058f, 0.41960f),
	glm::vec3(0.98039f, 0.98039f, 0.98039f),
	glm::vec3(0.45882f, 0.51764f, 0.56470f),
	glm::vec3(0.33725f, 0.42745f, 0.42745f),
	glm::vec3(0.25490f, 0.25490f, 0.25490f),
	glm::vec3(0.53333f, 0.44705f, 0.36862f),
	glm::vec3(0.47843f, 0.41960f, 0.38039f),
	glm::vec3(0.48235f, 0.47058f, 0.41960f)
};

const glm::vec3 houseColors[] = {
	glm::vec3(0.91764f, 0.89411f, 0.89803f),
	glm::vec3(0.70980f, 0.55686f, 0.52549f),
	glm::vec3(0.80784f, 0.62745f, 0.54117f),
	glm::vec3(0.29803f, 0.28235f, 0.34117f),
	glm::vec3(0.80392f, 0.76078f, 0.71372f),
	glm::vec3(0.56078f, 0.41960f, 0.43529f),
	glm::vec3(0.57254f, 0.71764f, 0.78431f),
	glm::vec3(0.93725f, 0.93333f, 0.90588f),
	glm::vec3(0.70980f, 0.69411f, 0.66274f),
	glm::vec3(0.83921f, 0.80392f, 0.78431f),
	glm::vec3(0.67058f, 0.63137f, 0.60000f),
	glm::vec3(0.57254f, 0.47058f, 0.48235f),
	glm::vec3(0.89411f, 0.76470f, 0.66274f),
	glm::vec3(0.76078f, 0.52549f, 0.49019f),
	glm::vec3(0.81568f, 0.51764f, 0.41568f),
	glm::vec3(0.72941f, 0.49803f, 0.39215f),
	glm::vec3(0.85098f, 0.75686f, 0.67450f),
	glm::vec3(0.67843f, 0.52156f, 0.48235f),
	glm::vec3(0.88627f, 0.74901f, 0.70196f),
	glm::vec3(0.98823f, 0.95686f, 0.90588f)

};

const glm::vec3 roofColors[] = {
	glm::vec3(157.0f / 255.0f, 99.0f / 255.0f, 77.0f / 255.0f),
	glm::vec3(57.0f / 255.0f, 61.0f / 255.0f, 69.0f / 255.0f),
	glm::vec3(126.0f / 255.0f, 145.0f / 255.0f, 152.0f / 255.0f),
	glm::vec3(85.0f / 255.0f, 95.0f / 255.0f, 103.0f / 255.0f),
	glm::vec3(127.0f / 255.0f, 53.0f / 255.0f, 44.0f / 255.0f),
	glm::vec3(205.0f / 255.0f, 210.0f / 255.0f, 214.0f / 255.0f)
};

//Size of house texture
const int HOUSETEXTURESIZE = 512;

class House : public Chunk
{
	GLuint* wallTex;
	GLuint* lightTex;
	GLuint* roofTex;
public:
	void render();
	House(Plot &plot);
};