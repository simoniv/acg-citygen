#pragma once

#include "Chunk.h"

class Water : public Chunk
{
public:
	Water(float minX, float minY, float maxX, float maxY);
};