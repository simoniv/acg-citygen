#include <string>

#include "Houses.h"
#include "../io/ConsolePrinter.h"

/**
 * Generate textures for the houses
 */
void Houses::generateTextures()
{
	for (int q = 0; q < 4; q++) {
		int xMin = 0;
		int xMax = 7;
		int yMin = 0;
		int yMax = 7;

		if (q == 1) {
			xMin = 1;
		}
		else if (q == 2) {
			xMin = 1;
			xMax = 6;
		}
		else if (q == 3) {
			xMin = 1;
			xMax = 6;
			yMin = 1;
			yMax = 6;
		}
		unsigned char textureMap[HOUSETEXTURESIZE*HOUSETEXTURESIZE * 3];

		for (int i = 0; i < HOUSETEXTURESIZE*HOUSETEXTURESIZE * 3; i += 3) {
			int x = i / 3;
			x = x % HOUSETEXTURESIZE;
			int y = ((i / 3) - x) / HOUSETEXTURESIZE;
			if (y % 8 <= yMin || y % 8 >= yMax || x % 8 <= xMin || x % 8 >= xMax) {
				textureMap[i] = 190;
				textureMap[i + 1] = 190;
				textureMap[i + 2] = 190;
			}
			else {
				int colOffsetChance = rand() % ((y + 1) % 8);
				int colOffset = 0.0f;
				if (colOffsetChance == 0) {
					colOffset = rand() % 100;
				}
				textureMap[i] = 20 + colOffset;
				textureMap[i + 1] = 20 + colOffset;
				textureMap[i + 2] = 20 + colOffset;
			}
		}

		GLuint texture;
		glGenTextures(1, &texture);

		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, HOUSETEXTURESIZE, HOUSETEXTURESIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, textureMap);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);

		Chunk::textures.push_back(texture);

		//LightMap

		int windowLightStrength[(HOUSETEXTURESIZE / 8) * (HOUSETEXTURESIZE / 8)];

		for (int i = 0; i < (HOUSETEXTURESIZE / 8) * (HOUSETEXTURESIZE / 8); i++) {
			windowLightStrength[i] = rand() % 9;
		}

		for (int i = 0; i < HOUSETEXTURESIZE*HOUSETEXTURESIZE * 3; i += 3) {
			int x = i / 3;
			x = x % HOUSETEXTURESIZE;
			int y = ((i / 3) - x) / HOUSETEXTURESIZE;

			int windowX = (x - x % 8) / 8;
			int windowY = (y - y % 8) / 8;
			int currentWindow = windowY * 64 + windowX;
			float currentWindowStrength;

			if ((float)windowLightStrength[currentWindow] / 8.0f == 1.0f) {
				currentWindowStrength = 1;
			}
			else {
				currentWindowStrength = windowLightStrength[currentWindow] * 0.04f;
			}

			if (y % 8 <= yMin || y % 8 >= yMax || x % 8 <= xMin || x % 8 >= xMax) {
				textureMap[i] = 0;
				textureMap[i + 1] = 0;
				textureMap[i + 2] = 0;
			}
			else {

				int colOffsetChance = rand() % ((y + 1) % 8);
				float colOffsetRed = 1.0f;
				float colOffsetGreen = 1.0f;
				float colOffsetBlue = 1.0f;

				if (colOffsetChance == 0) {
					colOffsetRed = ((float)(rand() % 10))*0.1f;
					colOffsetGreen = ((float)(rand() % 10))*0.1f;
					colOffsetBlue = ((float)(rand() % 10))*0.1f;
				}

				textureMap[i] = (int)(220.0f*currentWindowStrength*colOffsetRed + 1);
				textureMap[i + 1] = (int)(210.0f*currentWindowStrength*colOffsetGreen + 1);
				textureMap[i + 2] = (int)(180.0f*currentWindowStrength*colOffsetBlue + 1);
			}
		}


		GLuint textureLightMap;
		glGenTextures(1, &textureLightMap);

		glBindTexture(GL_TEXTURE_2D, textureLightMap);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, HOUSETEXTURESIZE, HOUSETEXTURESIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, textureMap);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);

		Chunk::textures.push_back(textureLightMap);
	}

}

/**
 * Makes parcels and creates houses
 */
void Houses::generateChunks(std::vector<Plot> &plots) {
	generateTextures();


	Chunk* tempChunk;
	for (int parcelNr = 0; parcelNr < plots.size(); parcelNr++) {
		tempChunk = new House(plots[parcelNr]);
		if (tempChunk->size() > 0) {
			chunkList.push_back(tempChunk);
		}
		else {
			delete tempChunk;
		}

		ConsolePrinter::printProgress("Making houses...", round(((float)parcelNr / (float)plots.size())*100.0f));


	}
	ConsolePrinter::printProgress("Making houses...", 100);
	ConsolePrinter::progressFinished();
	ConsolePrinter::printString("Done!");

}