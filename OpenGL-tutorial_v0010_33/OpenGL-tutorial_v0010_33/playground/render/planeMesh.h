#pragma once

#include <vector>
#include <iostream>

#include "ChunkCollection.h"
#include "../generation/GridElement.h"

class PlaneMesh : public ChunkCollection
{
public:
	void generateChunks(std::vector<std::vector<GridElement>> &grid);
};