#include "House.h"
#include "Edge.h"

/**
 *	Renders a house
 */
void House::render()
{
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // glm::normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // glm::normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		2,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // glm::normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	if (hasTexture)
	{
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			3,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // glm::normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		GLuint hasTex = glGetUniformLocation(programID, "hasTexture");

		glUniform1i(hasTex, 1);

		GLuint texLocation = glGetUniformLocation(programID, "textureSampler");
		GLuint lightMapLocation = glGetUniformLocation(programID, "lightMapSampler");
		glUniform1i(texLocation, 0);
		glUniform1i(lightMapLocation, 1);

		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, *wallTex);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, *lightTex);

	}
	else
	{


		GLuint hasTex = glGetUniformLocation(programID, "hasTexture");

		glUniform1i(hasTex, 0);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	//Draw the map
	glDrawElements(GL_TRIANGLES, sizeOfIndex, GL_UNSIGNED_SHORT, (void*)0);
}

/**
 * Makes a house from a parcel
 */
House::House(Plot &plot)
{

	float houseHeight = rand() % 8 + 4;

	glm::vec3 tempTopRightCoord;
	glm::vec3 tempTopLeftCoord;
	glm::vec3 tempMidCoord;
	glm::vec3 tempBotLeftCoord;
	glm::vec3 tempBotRightCoord;

	glm::vec3 edge1;
	glm::vec3 edge2;
	glm::vec3 normal;

	glm::vec3 houseColor(0.75f, 0.75f, 0.75f);

	float minHouseHeight = -10.0f;
	float lowestPoint = -10.0f;

	hasTexture = 1;

	float windowSize = 8.0f / 512.0f;

	wallTex = &textures[0];
	lightTex = &textures[1];

	int skyscraperChance = 100;

	std::vector<GridElement*> parcelVector = plot.elementVector;

	//Find current house parcel number
	int currentParcelNumber = -2;
	for (int parcelElement = 0; parcelElement < parcelVector.size(); parcelElement++)
	{
		if (!*(parcelVector[parcelElement]->topEdgeToggle) &&
			!*(parcelVector[parcelElement]->botEdgeToggle) &&
			!*(parcelVector[parcelElement]->leftEdgeToggle) &&
			!*(parcelVector[parcelElement]->rightEdgeToggle) &&
			!parcelVector[parcelElement]->nwEdgeToggle &&
			!parcelVector[parcelElement]->neEdgeToggle &&
			!parcelVector[parcelElement]->swEdgeToggle &&
			!parcelVector[parcelElement]->seEdgeToggle) {
			currentParcelNumber = parcelVector[parcelElement]->topTriangleParcelNumber;
		}
	}

	if (currentParcelNumber == -2)
	{
		generateVBO();
		return;
	}


	//Check bottom of house
	for (int parcelElement = 0; parcelElement < parcelVector.size(); parcelElement++)
	{
		if (parcelVector[parcelElement]->botLeftCoord->y > minHouseHeight)
		{
			minHouseHeight = parcelVector[parcelElement]->botLeftCoord->y;
		}
		if (parcelVector[parcelElement]->topLeftCoord->y > minHouseHeight)
		{
			minHouseHeight = parcelVector[parcelElement]->topLeftCoord->y;
		}
		if (parcelVector[parcelElement]->botRightCoord->y > minHouseHeight)
		{
			minHouseHeight = parcelVector[parcelElement]->botRightCoord->y;
		}
		if (parcelVector[parcelElement]->topRightCoord->y > minHouseHeight)
		{
			minHouseHeight = parcelVector[parcelElement]->topRightCoord->y;
		}
		if (parcelVector[parcelElement]->midCoord->y > minHouseHeight)
		{
			minHouseHeight = parcelVector[parcelElement]->midCoord->y;
		}


		if (parcelVector[parcelElement]->botLeftCoord->y < minHouseHeight)
		{
			lowestPoint = parcelVector[parcelElement]->botLeftCoord->y;
		}
		if (parcelVector[parcelElement]->topLeftCoord->y < minHouseHeight)
		{
			lowestPoint = parcelVector[parcelElement]->topLeftCoord->y;
		}
		if (parcelVector[parcelElement]->botRightCoord->y < minHouseHeight)
		{
			lowestPoint = parcelVector[parcelElement]->botRightCoord->y;
		}
		if (parcelVector[parcelElement]->topRightCoord->y < minHouseHeight)
		{
			lowestPoint = parcelVector[parcelElement]->topRightCoord->y;
		}
		if (parcelVector[parcelElement]->midCoord->y < minHouseHeight)
		{
			lowestPoint = parcelVector[parcelElement]->midCoord->y;
		}
	}

	//Set house height
	if (parcelVector[0]->mainAreaType == CITYAREA)
	{
		skyscraperChance = rand() % 10;
		if (skyscraperChance == 0 && parcelVector.size() > 50)
		{
			houseHeight = minHouseHeight + 10 + rand() % 50;
			wallTex = &textures[0];
			lightTex = &textures[1];
			houseColor = skyscraperColors[rand() % 8];
		}
		else if (parcelVector.size() < 25)
		{
			houseHeight = minHouseHeight + 2 + rand() % 2;
			int texNr = rand() % 4;
			wallTex = &textures[texNr * 2];
			lightTex = &textures[texNr * 2 + 1];
			houseColor = houseColors[rand() % 20];
		}
		else
		{
			houseHeight = minHouseHeight + 4 + rand() % 10;
			int texNr = (rand() % 3) + 1;
			wallTex = &textures[texNr * 2];
			lightTex = &textures[texNr * 2 + 1];
			houseColor = houseColors[rand() % 20];
		}
	}
	else if (parcelVector[0]->mainAreaType == INDUSTRIALAREA)
	{
		if (parcelVector.size() < 25)
		{
			houseHeight = minHouseHeight + 2 + rand() % 2;
			int texNr = (rand() % 3) + 1;
			wallTex = &textures[texNr * 2];
			lightTex = &textures[texNr * 2 + 1];
			houseColor = houseColors[rand() % 20];
		}
		else
		{
			houseHeight = minHouseHeight + 2 + rand() % 4;
			int texNr = (rand() % 3) + 1;
			wallTex = &textures[texNr * 2];
			lightTex = &textures[texNr * 2 + 1];
			houseColor = houseColors[rand() % 20];
		}
	}
	else
	{
		if (parcelVector.size() < 25) {
			houseHeight = minHouseHeight + 2 + rand() % 2;
			int texNr = (rand() % 3) + 1;
			wallTex = &textures[texNr * 2];
			lightTex = &textures[texNr * 2 + 1];
			houseColor = houseColors[rand() % 20];
		}
		else
		{
			houseHeight = minHouseHeight + 2 + rand() % 10;
			int texNr = (rand() % 3) + 1;
			wallTex = &textures[texNr * 2];
			lightTex = &textures[texNr * 2 + 1];
			houseColor = houseColors[rand() % 20];
		}
	}


	std::vector<Edge> edgeVector;

	//Make walls. Step 1. Find the edges that make up the walls. (all of this is done to know what the "outside" of the house is for correct normals and faces)
	for (int parcelElement = 0; parcelElement < parcelVector.size(); parcelElement++)
	{
		if (*(parcelVector[parcelElement]->leftEdgeToggle) && (parcelVector[parcelElement]->leftTriangleParcelNumber == currentParcelNumber))
		{
			edgeVector.push_back(Edge(parcelVector[parcelElement]->topLeftCoord, parcelVector[parcelElement]->botLeftCoord));
		}

		if (*(parcelVector[parcelElement]->rightEdgeToggle) && (parcelVector[parcelElement]->rightTriangleParcelNumber == currentParcelNumber))
		{
			edgeVector.push_back(Edge(parcelVector[parcelElement]->topRightCoord, parcelVector[parcelElement]->botRightCoord));
		}

		if (*(parcelVector[parcelElement]->topEdgeToggle) && (parcelVector[parcelElement]->topTriangleParcelNumber == currentParcelNumber))
		{
			edgeVector.push_back(Edge(parcelVector[parcelElement]->topLeftCoord, parcelVector[parcelElement]->topRightCoord));

		}

		if (*(parcelVector[parcelElement]->botEdgeToggle) && (parcelVector[parcelElement]->botTriangleParcelNumber == currentParcelNumber))
		{
			edgeVector.push_back(Edge(parcelVector[parcelElement]->botRightCoord, parcelVector[parcelElement]->botLeftCoord));

		}

		if (parcelVector[parcelElement]->nwEdgeToggle && (parcelVector[parcelElement]->topTriangleParcelNumber == currentParcelNumber
			|| parcelVector[parcelElement]->leftTriangleParcelNumber == currentParcelNumber))
		{
			edgeVector.push_back(Edge(parcelVector[parcelElement]->midCoord, parcelVector[parcelElement]->topLeftCoord));

		}

		if (parcelVector[parcelElement]->neEdgeToggle && (parcelVector[parcelElement]->topTriangleParcelNumber == currentParcelNumber
			|| parcelVector[parcelElement]->rightTriangleParcelNumber == currentParcelNumber))
		{
			edgeVector.push_back(Edge(parcelVector[parcelElement]->midCoord, parcelVector[parcelElement]->topRightCoord));

		}

		if (parcelVector[parcelElement]->swEdgeToggle && (parcelVector[parcelElement]->botTriangleParcelNumber == currentParcelNumber
			|| parcelVector[parcelElement]->leftTriangleParcelNumber == currentParcelNumber))
		{
			edgeVector.push_back(Edge(parcelVector[parcelElement]->midCoord, parcelVector[parcelElement]->botLeftCoord));

		}
		if (parcelVector[parcelElement]->seEdgeToggle && (parcelVector[parcelElement]->botTriangleParcelNumber == currentParcelNumber
			|| parcelVector[parcelElement]->rightTriangleParcelNumber == currentParcelNumber))
		{
			edgeVector.push_back(Edge(parcelVector[parcelElement]->midCoord, parcelVector[parcelElement]->botRightCoord));

		}

	}
	//Step 2. Sort the edges to form a continuous loop
	std::vector<Edge> sortedEdgeVector;
	sortedEdgeVector.push_back(edgeVector[0]);
	edgeVector.erase(edgeVector.begin());
	int currentPosition = 0;

	while (edgeVector.size() > 0)
	{
		currentPosition = sortedEdgeVector.size() - 1;
		for (std::vector<Edge>::iterator it = edgeVector.begin(); it != edgeVector.end(); ++it)
		{
			if (*((*it).firstVec) == *(sortedEdgeVector[currentPosition].secondVec))
			{
				sortedEdgeVector.push_back((*it));
				edgeVector.erase(it);
				break;
			}
			else if (*((*it).secondVec) == *(sortedEdgeVector[currentPosition].secondVec))
			{
				(*it).swap();
				sortedEdgeVector.push_back((*it));
				edgeVector.erase(it);
				break;
			}
		}
		if (currentPosition == sortedEdgeVector.size() - 1)
		{
			/*
			std::cout << "Edge std::vector: " << edgeVector.size() << "\n";

			for(int i = 0; i < edgeVector.size(); i++){
				std::cout << "[" << (*edgeVector[i].firstVec).x << ","  << (*edgeVector[i].firstVec).z << " ; " << (*edgeVector[i].secondVec).x << "," << (*edgeVector[i].secondVec).z << "]\n";
			}
			std::cout << "\nSorted Edge std::vector: " << sortedEdgeVector.size() << "\n";
			for(int i = 0; i < sortedEdgeVector.size(); i++){
				std::cout << "[" << (*sortedEdgeVector[i].firstVec).x << "," <<  (*sortedEdgeVector[i].firstVec).z << " ; " << (*sortedEdgeVector[i].secondVec).x << "," << (*sortedEdgeVector[i].secondVec).z << "]\n";
			}*/
			//TODO: Somehow two buildings in one parcel, ignore second building (for now)
			break;
		}
	}


	//Step 3. Count right- and left turns to establish if loop is clockwise or counter-clockwise
	int leftTurns = 0;
	int rightTurns = 0;
	//int firstTurnIndex = -1; 
	std::vector<int> turnIndexer; //Used to find the corners to make it easier to render better walls
	for (int i = 0; i < sortedEdgeVector.size(); i++)
	{
		glm::vec3 tempVec1;
		glm::vec3 tempVec2;
		glm::vec3 tempVecNormal;

		tempVec1 = *(sortedEdgeVector[i].secondVec) - *(sortedEdgeVector[i].firstVec);
		tempVec1.y = 0;
		glm::normalize(tempVec1);

		if (i == sortedEdgeVector.size() - 1)
		{
			tempVec2 = *(sortedEdgeVector[0].secondVec) - *(sortedEdgeVector[0].firstVec);
		}
		else {
			tempVec2 = *(sortedEdgeVector[i + 1].secondVec) - *(sortedEdgeVector[i + 1].firstVec);
		}
		tempVec2.y = 0;
		glm::normalize(tempVec2);

		tempVecNormal = glm::cross(tempVec1, tempVec2);

		if (tempVecNormal.y == 0)
		{
		}
		else if (tempVecNormal.y < 0)
		{
			leftTurns++;
			if (i == sortedEdgeVector.size() - 1)
			{
				turnIndexer.push_back(0);
			}
			else
			{
				turnIndexer.push_back(i + 1);
			}
		}
		else
		{
			rightTurns++;
			if (i == sortedEdgeVector.size() - 1)
			{
				turnIndexer.push_back(0);
			}
			else
			{
				turnIndexer.push_back(i + 1);
			}
		}

	}

	//Probably strange looking building, toss
	//if(min(leftTurns, rightTurns) >= 3){
	//	generateVBO();
	//	return;
	//}

	//Step 4. Make a single edge per wall
	std::vector<Edge> longEdgeVector;
	for (int i = 0; i < turnIndexer.size(); i++)
	{
		Edge tempEdge;
		tempEdge.firstVec = sortedEdgeVector[turnIndexer[i]].firstVec;

		if (i == turnIndexer.size() - 1) //If at the end of the indexer, use the first index for data
		{
			if (turnIndexer[0] - 1 < 0) //If index happen to be 0
			{
				tempEdge.secondVec = sortedEdgeVector[sortedEdgeVector.size() - 1].secondVec;
			}
			else
			{
				tempEdge.secondVec = sortedEdgeVector[turnIndexer[0] - 1].secondVec;
			}
		}
		else {
			if (turnIndexer[i + 1] - 1 < 0) //If index happen to be 0
			{
				tempEdge.secondVec = sortedEdgeVector[sortedEdgeVector.size() - 1].secondVec;
			}
			else
			{
				tempEdge.secondVec = sortedEdgeVector[turnIndexer[i + 1] - 1].secondVec;
			}

		}
		longEdgeVector.push_back(tempEdge);

	}



	//Step 5. Make sure all loops are the same direction
	if (leftTurns < rightTurns)
	{
		for (int i = 0; i < longEdgeVector.size(); i++)
		{
			longEdgeVector[i].swap();
		}
	}

	//Make the walls.
	for (int i = 0; i < longEdgeVector.size(); i++)
	{

		std::vector<glm::vec3> tempVerts;
		std::vector<glm::vec3> tempCols;
		std::vector<glm::vec3> tempNorms;
		std::vector<glm::vec2> tempUVs;

		float horizontalDistance = (float)ceil(glm::distance(*(longEdgeVector[i].firstVec), *(longEdgeVector[i].secondVec)));
		float verticalDistance = (float)ceil(houseHeight - lowestPoint);

		float horizontalOffset = ((rand() % 64)*windowSize);
		float verticalOffset = ((rand() % 64)*windowSize);

		horizontalDistance = horizontalDistance * 2.0f*windowSize;
		verticalDistance = verticalDistance * 2.0f*windowSize;

		tempBotLeftCoord = *(longEdgeVector[i].firstVec);
		tempBotLeftCoord.y = lowestPoint;

		tempBotRightCoord = *(longEdgeVector[i].secondVec);
		tempBotRightCoord.y = lowestPoint;

		tempMidCoord = *(longEdgeVector[i].firstVec);
		tempMidCoord.y = houseHeight;

		tempVerts.push_back(tempBotLeftCoord);
		tempVerts.push_back(tempMidCoord);
		tempVerts.push_back(tempBotRightCoord);

		tempCols.push_back(houseColor);
		tempCols.push_back(houseColor);
		tempCols.push_back(houseColor);

		edge1 = (tempBotRightCoord)-(tempMidCoord);
		edge2 = (tempBotLeftCoord)-(tempMidCoord);
		normal = glm::cross(edge1, edge2);
		glm::normalize(normal);

		tempNorms.push_back(normal);
		tempNorms.push_back(normal);
		tempNorms.push_back(normal);

		tempUVs.push_back(glm::vec2(0.0f + horizontalOffset, 0.0f + verticalOffset));
		tempUVs.push_back(glm::vec2(0.0f + horizontalOffset, verticalDistance + verticalOffset));
		tempUVs.push_back(glm::vec2(horizontalDistance + horizontalOffset, 0.0f + verticalOffset));

		addTriangle(tempVerts, tempCols, tempNorms, tempUVs);

		tempVerts.clear();
		tempCols.clear();
		tempNorms.clear();
		tempUVs.clear();

		tempBotLeftCoord = *(longEdgeVector[i].firstVec);
		tempBotLeftCoord.y = houseHeight;

		tempBotRightCoord = *(longEdgeVector[i].secondVec);
		tempBotRightCoord.y = lowestPoint;

		tempMidCoord = *(longEdgeVector[i].secondVec);
		tempMidCoord.y = houseHeight;

		tempVerts.push_back(tempBotLeftCoord);
		tempVerts.push_back(tempMidCoord);
		tempVerts.push_back(tempBotRightCoord);

		tempCols.push_back(houseColor);
		tempCols.push_back(houseColor);
		tempCols.push_back(houseColor);

		edge1 = (tempBotRightCoord)-(tempMidCoord);
		edge2 = (tempBotLeftCoord)-(tempMidCoord);
		normal = glm::cross(edge1, edge2);
		glm::normalize(normal);

		tempNorms.push_back(normal);
		tempNorms.push_back(normal);
		tempNorms.push_back(normal);

		tempUVs.push_back(glm::vec2(0.0f + horizontalOffset, verticalDistance + verticalOffset));
		tempUVs.push_back(glm::vec2(horizontalDistance + horizontalOffset, verticalDistance + verticalOffset));
		tempUVs.push_back(glm::vec2(horizontalDistance + horizontalOffset, 0.0f + verticalOffset));

		addTriangle(tempVerts, tempCols, tempNorms, tempUVs);
	}


	//Make the roof
	if (skyscraperChance != 0)
	{
		houseColor = roofColors[rand() % 6];
	}

	if (longEdgeVector.size() == 3)
	{
		std::vector<glm::vec3> tempVerts;
		std::vector<glm::vec3> tempCols;
		std::vector<glm::vec3> tempNorms;
		std::vector<glm::vec2> tempUVs;

		tempBotLeftCoord = *(longEdgeVector[0].firstVec);
		tempBotLeftCoord.y = houseHeight;

		tempBotRightCoord = *(longEdgeVector[0].secondVec);
		tempBotRightCoord.y = houseHeight;

		tempMidCoord = *(longEdgeVector[1].secondVec);
		tempMidCoord.y = houseHeight;

		tempVerts.push_back(tempBotRightCoord);
		tempVerts.push_back(tempBotLeftCoord);
		tempVerts.push_back(tempMidCoord);

		tempCols.push_back(houseColor);
		tempCols.push_back(houseColor);
		tempCols.push_back(houseColor);

		edge1 = (tempBotRightCoord)-(tempMidCoord);
		edge2 = (tempBotLeftCoord)-(tempMidCoord);
		normal = glm::cross(edge2, edge1);
		glm::normalize(normal);

		tempNorms.push_back(normal);
		tempNorms.push_back(normal);
		tempNorms.push_back(normal);

		tempUVs.push_back(glm::vec2(0.0f, 0.0f));
		tempUVs.push_back(glm::vec2(0.0f, 0.0f));
		tempUVs.push_back(glm::vec2(0.0f, 0.0f));

		addTriangle(tempVerts, tempCols, tempNorms, tempUVs);

	}
	else if (leftTurns == 0 || rightTurns == 0) //If roof makes convex polygon
	{
		for (int i = 1; i < longEdgeVector.size(); i++)
		{
			std::vector<glm::vec3> tempVerts;
			std::vector<glm::vec3> tempCols;
			std::vector<glm::vec3> tempNorms;
			std::vector<glm::vec2> tempUVs;

			tempBotLeftCoord = *(longEdgeVector[0].firstVec);
			tempBotLeftCoord.y = houseHeight;

			tempBotRightCoord = *(longEdgeVector[i].secondVec);
			tempBotRightCoord.y = houseHeight;

			tempMidCoord = *(longEdgeVector[i].firstVec);
			tempMidCoord.y = houseHeight;

			tempVerts.push_back(tempBotLeftCoord);
			tempVerts.push_back(tempBotRightCoord);
			tempVerts.push_back(tempMidCoord);

			tempCols.push_back(houseColor);
			tempCols.push_back(houseColor);
			tempCols.push_back(houseColor);

			edge1 = (tempBotRightCoord)-(tempMidCoord);
			edge2 = (tempBotLeftCoord)-(tempMidCoord);
			normal = glm::cross(edge2, edge1);
			glm::normalize(normal);

			tempNorms.push_back(normal);
			tempNorms.push_back(normal);
			tempNorms.push_back(normal);

			tempUVs.push_back(glm::vec2(0.0f, 0.0f));
			tempUVs.push_back(glm::vec2(0.0f, 0.0f));
			tempUVs.push_back(glm::vec2(0.0f, 0.0f));

			addTriangle(tempVerts, tempCols, tempNorms, tempUVs);
		}

	}
	else //If roof is concave polygon :(
	{


		for (int parcelElement = 0; parcelElement < parcelVector.size(); parcelElement++)
		{
			if (parcelVector[parcelElement]->topTriangleParcelNumber == currentParcelNumber
				&& parcelVector[parcelElement]->leftTriangleParcelNumber == currentParcelNumber
				&& parcelVector[parcelElement]->rightTriangleParcelNumber == currentParcelNumber
				&& parcelVector[parcelElement]->botTriangleParcelNumber == currentParcelNumber)
			{
				std::vector<glm::vec3> tempVerts;
				std::vector<glm::vec3> tempCols;
				std::vector<glm::vec3> tempNorms;
				std::vector<glm::vec2> tempUVs;

				tempTopRightCoord = *(parcelVector[parcelElement]->botRightCoord);
				tempTopRightCoord.y = houseHeight;

				tempTopLeftCoord = *(parcelVector[parcelElement]->topRightCoord);
				tempTopLeftCoord.y = houseHeight;

				tempMidCoord = *(parcelVector[parcelElement]->topLeftCoord);
				tempMidCoord.y = houseHeight;


				tempVerts.push_back(tempTopRightCoord);
				tempVerts.push_back(tempTopLeftCoord);
				tempVerts.push_back(tempMidCoord);

				tempCols.push_back(houseColor);
				tempCols.push_back(houseColor);
				tempCols.push_back(houseColor);

				edge1 = (tempTopRightCoord)-(tempMidCoord);
				edge2 = (tempTopLeftCoord)-(tempMidCoord);
				normal = glm::cross(edge1, edge2);
				glm::normalize(normal);

				tempNorms.push_back(normal);
				tempNorms.push_back(normal);
				tempNorms.push_back(normal);

				tempUVs.push_back(glm::vec2(0.0f, 0.0f));
				tempUVs.push_back(glm::vec2(0.0f, 0.0f));
				tempUVs.push_back(glm::vec2(0.0f, 0.0f));

				addTriangle(tempVerts, tempCols, tempNorms, tempUVs);

				tempVerts.clear();
				tempCols.clear();
				tempNorms.clear();
				tempUVs.clear();

				tempTopRightCoord = *(parcelVector[parcelElement]->botRightCoord);
				tempTopRightCoord.y = houseHeight;

				tempTopLeftCoord = *(parcelVector[parcelElement]->topLeftCoord);
				tempTopLeftCoord.y = houseHeight;

				tempMidCoord = *(parcelVector[parcelElement]->botLeftCoord);
				tempMidCoord.y = houseHeight;


				tempVerts.push_back(tempTopRightCoord);
				tempVerts.push_back(tempTopLeftCoord);
				tempVerts.push_back(tempMidCoord);

				tempCols.push_back(houseColor);
				tempCols.push_back(houseColor);
				tempCols.push_back(houseColor);

				edge1 = (tempTopRightCoord)-(tempMidCoord);
				edge2 = (tempTopLeftCoord)-(tempMidCoord);
				normal = glm::cross(edge1, edge2);
				glm::normalize(normal);

				tempNorms.push_back(normal);
				tempNorms.push_back(normal);
				tempNorms.push_back(normal);

				tempUVs.push_back(glm::vec2(0.0f, 0.0f));
				tempUVs.push_back(glm::vec2(0.0f, 0.0f));
				tempUVs.push_back(glm::vec2(0.0f, 0.0f));

				addTriangle(tempVerts, tempCols, tempNorms, tempUVs);

			}
			else
			{
				if (parcelVector[parcelElement]->topTriangleParcelNumber == currentParcelNumber)
				{
					//Top triangle
					std::vector<glm::vec3> tempVerts;
					std::vector<glm::vec3> tempCols;
					std::vector<glm::vec3> tempNorms;
					std::vector<glm::vec2> tempUVs;

					tempTopRightCoord = *(parcelVector[parcelElement]->topRightCoord);
					tempTopRightCoord.y = houseHeight;

					tempTopLeftCoord = *(parcelVector[parcelElement]->topLeftCoord);
					tempTopLeftCoord.y = houseHeight;

					tempMidCoord = *(parcelVector[parcelElement]->midCoord);
					tempMidCoord.y = houseHeight;


					tempVerts.push_back(tempTopRightCoord);
					tempVerts.push_back(tempTopLeftCoord);
					tempVerts.push_back(tempMidCoord);

					tempCols.push_back(houseColor);
					tempCols.push_back(houseColor);
					tempCols.push_back(houseColor);

					edge1 = (tempTopRightCoord)-(tempMidCoord);
					edge2 = (tempTopLeftCoord)-(tempMidCoord);
					normal = glm::cross(edge1, edge2);
					glm::normalize(normal);

					tempNorms.push_back(normal);
					tempNorms.push_back(normal);
					tempNorms.push_back(normal);

					tempUVs.push_back(glm::vec2(0.0f, 0.0f));
					tempUVs.push_back(glm::vec2(0.0f, 0.0f));
					tempUVs.push_back(glm::vec2(0.0f, 0.0f));

					addTriangle(tempVerts, tempCols, tempNorms, tempUVs);


				}


				if (parcelVector[parcelElement]->leftTriangleParcelNumber == currentParcelNumber)
				{
					//Left triangle
					std::vector<glm::vec3> tempVerts;
					std::vector<glm::vec3> tempCols;
					std::vector<glm::vec3> tempNorms;
					std::vector<glm::vec2> tempUVs;

					tempTopLeftCoord = *(parcelVector[parcelElement]->topLeftCoord);
					tempTopLeftCoord.y = houseHeight;

					tempBotLeftCoord = *(parcelVector[parcelElement]->botLeftCoord);
					tempBotLeftCoord.y = houseHeight;

					tempMidCoord = *(parcelVector[parcelElement]->midCoord);
					tempMidCoord.y = houseHeight;

					tempVerts.push_back(tempTopLeftCoord);
					tempVerts.push_back(tempBotLeftCoord);
					tempVerts.push_back(tempMidCoord);

					tempCols.push_back(houseColor);
					tempCols.push_back(houseColor);
					tempCols.push_back(houseColor);

					edge1 = (tempBotLeftCoord)-(tempMidCoord);
					edge2 = (tempTopLeftCoord)-(tempMidCoord);
					normal = glm::cross(edge2, edge1);
					glm::normalize(normal);

					tempNorms.push_back(normal);
					tempNorms.push_back(normal);
					tempNorms.push_back(normal);

					tempUVs.push_back(glm::vec2(0.0f, 0.0f));
					tempUVs.push_back(glm::vec2(0.0f, 0.0f));
					tempUVs.push_back(glm::vec2(0.0f, 0.0f));

					addTriangle(tempVerts, tempCols, tempNorms, tempUVs);


				}


				if (parcelVector[parcelElement]->rightTriangleParcelNumber == currentParcelNumber)
				{
					//Right triangle
					std::vector<glm::vec3> tempVerts;
					std::vector<glm::vec3> tempCols;
					std::vector<glm::vec3> tempNorms;
					std::vector<glm::vec2> tempUVs;

					tempBotRightCoord = *(parcelVector[parcelElement]->botRightCoord);
					tempBotRightCoord.y = houseHeight;

					tempTopRightCoord = *(parcelVector[parcelElement]->topRightCoord);
					tempTopRightCoord.y = houseHeight;

					tempMidCoord = *(parcelVector[parcelElement]->midCoord);
					tempMidCoord.y = houseHeight;

					tempVerts.push_back(tempBotRightCoord);
					tempVerts.push_back(tempTopRightCoord);
					tempVerts.push_back(tempMidCoord);

					tempCols.push_back(houseColor);
					tempCols.push_back(houseColor);
					tempCols.push_back(houseColor);

					edge1 = (tempBotRightCoord)-(tempMidCoord);
					edge2 = (tempTopRightCoord)-(tempMidCoord);
					normal = glm::cross(edge1, edge2);
					glm::normalize(normal);

					tempNorms.push_back(normal);
					tempNorms.push_back(normal);
					tempNorms.push_back(normal);

					tempUVs.push_back(glm::vec2(0.0f, 0.0f));
					tempUVs.push_back(glm::vec2(0.0f, 0.0f));
					tempUVs.push_back(glm::vec2(0.0f, 0.0f));

					addTriangle(tempVerts, tempCols, tempNorms, tempUVs);

				}

				if (parcelVector[parcelElement]->botTriangleParcelNumber == currentParcelNumber)
				{
					//Bot triangle
					std::vector<glm::vec3> tempVerts;
					std::vector<glm::vec3> tempCols;
					std::vector<glm::vec3> tempNorms;
					std::vector<glm::vec2> tempUVs;

					tempBotLeftCoord = *(parcelVector[parcelElement]->botLeftCoord);
					tempBotLeftCoord.y = houseHeight;

					tempBotRightCoord = *(parcelVector[parcelElement]->botRightCoord);
					tempBotRightCoord.y = houseHeight;

					tempMidCoord = *(parcelVector[parcelElement]->midCoord);
					tempMidCoord.y = houseHeight;

					tempVerts.push_back(tempBotLeftCoord);
					tempVerts.push_back(tempBotRightCoord);
					tempVerts.push_back(tempMidCoord);

					tempCols.push_back(houseColor);
					tempCols.push_back(houseColor);
					tempCols.push_back(houseColor);

					edge1 = (tempBotRightCoord)-(tempMidCoord);
					edge2 = (tempBotLeftCoord)-(tempMidCoord);
					normal = glm::cross(edge2, edge1);
					glm::normalize(normal);

					tempNorms.push_back(normal);
					tempNorms.push_back(normal);
					tempNorms.push_back(normal);

					tempUVs.push_back(glm::vec2(0.0f, 0.0f));
					tempUVs.push_back(glm::vec2(0.0f, 0.0f));
					tempUVs.push_back(glm::vec2(0.0f, 0.0f));

					addTriangle(tempVerts, tempCols, tempNorms, tempUVs);

				}
			}

		}
	}



	generateVBO();


}