#include "PlaneChunk.h"


/**
 *	Smaller chunk making up the plane
 */
PlaneChunk::PlaneChunk(std::vector<std::vector<GridElement>> &grid, int chunkNr, int chunkSize, int chunkRes)
{

	int y = (chunkNr%chunkRes);
	int x = ((chunkNr - y) / chunkRes)*chunkSize;
	y = y * chunkSize;

	vertices.reserve(chunkSize*chunkSize * 4);
	colors.reserve(chunkSize*chunkSize * 4);
	normals.reserve(chunkSize*chunkSize * 4);

	hasTexture = 0;

	/*
	std::cout << "Generating chunk nr: " << chunkNr << "\n";
	std::cout << "Starting at: " << x << "," << y << "\n";
	std::cout << "Ending at: " << x + chunkSize << "," << y + chunkSize<< "\n";
	std::cout << "--------------------------\n";
	*/

	for (int i = x; i < x + chunkSize && i < grid.size(); i++)
	{
		for (int j = y; j < y + chunkSize && j < grid.size(); j++)
		{
			GridElement* currentSquare = &grid[i][j];



			//Making the normals
			glm::vec3 midNormal = glm::normalize(currentSquare->getBotTriangleNormal() +
				currentSquare->getTopTriangleNormal() +
				currentSquare->getLeftTriangleNormal() +
				currentSquare->getRightTriangleNormal());


			glm::vec3 topLeftNormal = currentSquare->getTopTriangleNormal() + currentSquare->getLeftTriangleNormal();
			if (i > 0)
			{
				topLeftNormal = topLeftNormal + grid[i - 1][j].getRightTriangleNormal() + grid[i - 1][j].getTopTriangleNormal();
			}
			if (j > 0)
			{
				topLeftNormal = topLeftNormal + grid[i][j - 1].getLeftTriangleNormal() + grid[i][j - 1].getBotTriangleNormal();
			}
			if (i > 0 && j > 0)
			{
				topLeftNormal = topLeftNormal + grid[i - 1][j - 1].getRightTriangleNormal() + grid[i - 1][j - 1].getBotTriangleNormal();
			}
			topLeftNormal = glm::normalize(topLeftNormal);


			glm::vec3 topRightNormal = currentSquare->getTopTriangleNormal() + currentSquare->getRightTriangleNormal();
			if (i < grid.size() - 1)
			{
				topRightNormal = topRightNormal + grid[i + 1][j].getLeftTriangleNormal() + grid[i + 1][j].getTopTriangleNormal();
			}
			if (j > 0)
			{
				topRightNormal = topRightNormal + grid[i][j - 1].getRightTriangleNormal() + grid[i][j - 1].getBotTriangleNormal();
			}
			if (i < grid.size() - 1 && j > 0)
			{
				topRightNormal = topRightNormal + grid[i + 1][j - 1].getLeftTriangleNormal() + grid[i + 1][j - 1].getBotTriangleNormal();
			}
			topRightNormal = glm::normalize(topRightNormal);


			glm::vec3 botLeftNormal = currentSquare->getBotTriangleNormal() + currentSquare->getLeftTriangleNormal();
			if (i > 0)
			{
				botLeftNormal = botLeftNormal + grid[i - 1][j].getRightTriangleNormal() + grid[i - 1][j].getBotTriangleNormal();
			}
			if (j < grid.size() - 1)
			{
				botLeftNormal = botLeftNormal + grid[i][j + 1].getTopTriangleNormal() + grid[i][j + 1].getLeftTriangleNormal();
			}
			if (i > 0 && j < grid.size() - 1)
			{
				botLeftNormal = botLeftNormal + grid[i - 1][j + 1].getTopTriangleNormal() + grid[i - 1][j + 1].getRightTriangleNormal();
			}
			botLeftNormal = glm::normalize(botLeftNormal);


			glm::vec3 botRightNormal = currentSquare->getBotTriangleNormal() + currentSquare->getRightTriangleNormal();
			if (i < grid.size() - 1)
			{
				botRightNormal = botRightNormal + grid[i + 1][j].getLeftTriangleNormal() + grid[i + 1][j].getBotTriangleNormal();
			}
			if (j < grid.size() - 1)
			{
				botRightNormal = botRightNormal + grid[i][j + 1].getTopTriangleNormal() + grid[i][j + 1].getRightTriangleNormal();
			}
			if (i < grid.size() - 1 && j < grid.size() - 1)
			{
				botRightNormal = botRightNormal + grid[i + 1][j + 1].getTopTriangleNormal() + grid[i + 1][j + 1].getLeftTriangleNormal();
			}
			botRightNormal = glm::normalize(botRightNormal);


			//Make graphical representation-----------------------------------------

			std::vector<glm::vec3> tempVerts;
			std::vector<glm::vec3> tempCols;
			std::vector<glm::vec3> tempNorms;

			//Top triangle
			tempVerts.push_back(*(currentSquare->topRightCoord));
			tempVerts.push_back(*(currentSquare->topLeftCoord));
			tempVerts.push_back(*(currentSquare->midCoord));

			if (currentSquare->topTriangleAreaType == ROAD || currentSquare->topTriangleAreaType == HIGHWAY)
			{
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
			}
			else if (currentSquare->topTriangleAreaType == ROADRESERVED || currentSquare->topTriangleAreaType == HIGHWAYRESERVED)
			{
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
			}
			else
			{
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
			}

			//glm::vec3 normal = currentSquare->getTopTriangleNormal();

			tempNorms.push_back(topRightNormal);
			tempNorms.push_back(topLeftNormal);
			tempNorms.push_back(midNormal);

			addTriangle(tempVerts, tempCols, tempNorms);

			tempVerts.clear();
			tempCols.clear();
			tempNorms.clear();


			//Left triangle
			tempVerts.push_back(*(currentSquare->topLeftCoord));
			tempVerts.push_back(*(currentSquare->botLeftCoord));
			tempVerts.push_back(*(currentSquare->midCoord));

			if (currentSquare->leftTriangleAreaType == ROAD || currentSquare->leftTriangleAreaType == HIGHWAY)
			{
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
			}
			else if (currentSquare->leftTriangleAreaType == ROADRESERVED || currentSquare->leftTriangleAreaType == HIGHWAYRESERVED)
			{
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
			}
			else
			{
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
			}

			//normal = currentSquare->getLeftTriangleNormal();

			tempNorms.push_back(topLeftNormal);
			tempNorms.push_back(botLeftNormal);
			tempNorms.push_back(midNormal);

			addTriangle(tempVerts, tempCols, tempNorms);

			tempVerts.clear();
			tempCols.clear();
			tempNorms.clear();


			//Right triangle
			tempVerts.push_back(*(currentSquare->botRightCoord));
			tempVerts.push_back(*(currentSquare->topRightCoord));
			tempVerts.push_back(*(currentSquare->midCoord));

			if (currentSquare->rightTriangleAreaType == ROAD || currentSquare->rightTriangleAreaType == HIGHWAY)
			{
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
			}
			else if (currentSquare->rightTriangleAreaType == ROADRESERVED || currentSquare->rightTriangleAreaType == HIGHWAYRESERVED)
			{
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
			}
			else
			{
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
			}

			//normal = currentSquare->getRightTriangleNormal();

			tempNorms.push_back(botRightNormal);
			tempNorms.push_back(topRightNormal);
			tempNorms.push_back(midNormal);

			addTriangle(tempVerts, tempCols, tempNorms);

			tempVerts.clear();
			tempCols.clear();
			tempNorms.clear();


			//Bot triangle
			tempVerts.push_back(*(currentSquare->botLeftCoord));
			tempVerts.push_back(*(currentSquare->botRightCoord));
			tempVerts.push_back(*(currentSquare->midCoord));

			if (currentSquare->botTriangleAreaType == ROAD || currentSquare->botTriangleAreaType == HIGHWAY)
			{
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
				tempCols.push_back(glm::vec3(0.7f, 0.7f, 0.7f));
			}
			else if (currentSquare->botTriangleAreaType == ROADRESERVED || currentSquare->botTriangleAreaType == HIGHWAYRESERVED)
			{
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
				tempCols.push_back(glm::vec3(0.45f, 0.65f, 0.45f));
			}
			else
			{
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
				tempCols.push_back(glm::vec3(0.4f, 0.6f, 0.4f));
			}

			//normal = currentSquare->getBotTriangleNormal();

			tempNorms.push_back(botLeftNormal);
			tempNorms.push_back(botRightNormal);
			tempNorms.push_back(midNormal);

			addTriangle(tempVerts, tempCols, tempNorms);


		}
	}

	generateVBO();
}