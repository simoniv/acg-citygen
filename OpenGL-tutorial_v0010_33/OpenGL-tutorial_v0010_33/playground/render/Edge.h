#pragma once

#include <glm/glm.hpp>

//Support struct for walls
struct Edge
{
	glm::vec3* firstVec;
	glm::vec3* secondVec;

	Edge(glm::vec3* vec1, glm::vec3* vec2)
	{
		firstVec = vec1;
		secondVec = vec2;
	}

	Edge() {}

	void swap()
	{
		glm::vec3* tempVec;
		tempVec = firstVec;
		firstVec = secondVec;
		secondVec = tempVec;
	}
};