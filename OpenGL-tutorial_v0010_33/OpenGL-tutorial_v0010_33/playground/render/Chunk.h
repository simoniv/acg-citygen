#pragma once

#include <vector>
#include <algorithm>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>

#include <GL/glew.h>

#include "common/controls.hpp"

class Chunk
{
public:
	static int triangleCount;
	static GLuint programID;
	static std::vector<GLuint> textures;

protected:
	GLuint vertexbuffer;
	GLuint normalbuffer;
	GLuint colorbuffer;
	GLuint uvbuffer;
	GLuint elementbuffer;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> colors;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> uvs;
	std::vector<unsigned short> indices;
	int sizeOfIndex;
	int sizeOfChunk;
	bool hasTexture;
	float xMin, yMin, zMin, xMax, yMax, zMax;
	virtual void addTriangle(const std::vector<glm::vec3>& triangleVertices, const std::vector<glm::vec3>& triangleColors, const std::vector<glm::vec3>& triangleNormals);
	virtual void addTriangle(const std::vector<glm::vec3>& triangleVertices, const std::vector<glm::vec3>& triangleColors, const std::vector<glm::vec3>& triangleNormals, const std::vector<glm::vec2>& triangleUVs);
	void generateVBO();
public:
	virtual void render();
	bool isInsideFrustum(PlaneFrustum* pl);
	int size()
	{
		return sizeOfChunk;
	};
};