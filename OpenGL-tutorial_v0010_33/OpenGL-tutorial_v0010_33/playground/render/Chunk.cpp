#include "Chunk.h"


int Chunk::triangleCount = 0;
GLuint Chunk::programID = 0;
std::vector<GLuint> Chunk::textures = std::vector<GLuint>();

/**
 *	Sets up the VBO's for the chunks
 */
void Chunk::generateVBO()
{
	sizeOfChunk = vertices.size();
	//chunk buffer initialization
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	//chunk color buffer initialization
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(glm::vec3), &colors[0], GL_STATIC_DRAW);

	//chunk normal buffer initialization
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

	if (hasTexture)
	{
		//chunk UV buffer initialization
		glGenBuffers(1, &uvbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	}

	sizeOfIndex = indices.size();
	//chunk index buffer init
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

	//Stupid std::vector deletion...
	std::vector<glm::vec3>().swap(vertices);
	vertices.clear();
	vertices.shrink_to_fit();

	std::vector<glm::vec3>().swap(normals);
	normals.clear();
	normals.shrink_to_fit();

	std::vector<glm::vec3>().swap(colors);
	colors.clear();
	colors.shrink_to_fit();

	std::vector<glm::vec2>().swap(uvs);
	uvs.clear();
	uvs.shrink_to_fit();

	std::vector<unsigned short>().swap(indices);
	indices.clear();
	indices.shrink_to_fit();
}


/**
 * Adds a triangle to the chunk, and updates indices if present.
 */
void Chunk::addTriangle(const std::vector<glm::vec3>& triangleVertices, const std::vector<glm::vec3>& triangleColors,const std::vector<glm::vec3>& triangleNormals)
{

	unsigned short index0 = 65535;
	unsigned short index1 = 65535;
	unsigned short index2 = 65535;

	if (vertices.size() == 0)
	{
		xMin = triangleVertices[0].x;
		xMax = triangleVertices[0].x;
		yMin = triangleVertices[0].y;
		yMax = triangleVertices[0].y;
		zMin = triangleVertices[0].z;
		zMax = triangleVertices[0].z;
	}

	for (int i = 0; i < vertices.size(); i++)
	{
		if (triangleVertices[0] == vertices[i] && triangleColors[0] == colors[i] && triangleNormals[0] == normals[i])
		{
			index0 = i;
		}

		if (triangleVertices[1] == vertices[i] && triangleColors[1] == colors[i] && triangleNormals[1] == normals[i])
		{
			index1 = i;
		}
		if (triangleVertices[2] == vertices[i] && triangleColors[2] == colors[i] && triangleNormals[2] == normals[i])
		{
			index2 = i;
		}
	}


	if (index0 != 65535)
	{
		indices.push_back(index0);
	}
	else
	{
		vertices.push_back(triangleVertices[0]);
		colors.push_back(triangleColors[0]);
		normals.push_back(triangleNormals[0]);
		indices.push_back((unsigned short)(vertices.size() - 1));

		xMin = std::min(xMin, triangleVertices[0].x);
		xMax = std::max(xMax, triangleVertices[0].x);
		yMin = std::min(yMin, triangleVertices[0].y);
		yMax = std::max(yMax, triangleVertices[0].y);
		zMin = std::min(zMin, triangleVertices[0].z);
		zMax = std::max(zMax, triangleVertices[0].z);
	}

	if (index1 != 65535)
	{
		indices.push_back(index1);
	}
	else
	{
		vertices.push_back(triangleVertices[1]);
		colors.push_back(triangleColors[1]);
		normals.push_back(triangleNormals[1]);
		indices.push_back((unsigned short)(vertices.size() - 1));

		xMin = std::min(xMin, triangleVertices[1].x);
		xMax = std::max(xMax, triangleVertices[1].x);
		yMin = std::min(yMin, triangleVertices[1].y);
		yMax = std::max(yMax, triangleVertices[1].y);
		zMin = std::min(zMin, triangleVertices[1].z);
		zMax = std::max(zMax, triangleVertices[1].z);
	}

	if (index2 != 65535)
	{
		indices.push_back(index2);
	}
	else
	{
		vertices.push_back(triangleVertices[2]);
		colors.push_back(triangleColors[2]);
		normals.push_back(triangleNormals[2]);
		indices.push_back((unsigned short)(vertices.size() - 1));

		xMin = std::min(xMin, triangleVertices[2].x);
		xMax = std::max(xMax, triangleVertices[2].x);
		yMin = std::min(yMin, triangleVertices[2].y);
		yMax = std::max(yMax, triangleVertices[2].y);
		zMin = std::min(zMin, triangleVertices[2].z);
		zMax = std::max(zMax, triangleVertices[2].z);
	}

	Chunk::triangleCount++;

}

/**
 * Adds a triangle to the chunk, and updates indices if present, with UV's
 */
void Chunk::addTriangle(const std::vector<glm::vec3>& triangleVertices, const std::vector<glm::vec3>& triangleColors, const std::vector<glm::vec3>& triangleNormals, const std::vector<glm::vec2>& triangleUVs)
{
	unsigned short index0 = 65535;
	unsigned short index1 = 65535;
	unsigned short index2 = 65535;

	if (vertices.size() == 0)
	{
		xMin = triangleVertices[0].x;
		xMax = triangleVertices[0].x;
		yMin = triangleVertices[0].y;
		yMax = triangleVertices[0].y;
		zMin = triangleVertices[0].z;
		zMax = triangleVertices[0].z;
	}

	for (int i = 0; i < vertices.size(); i++)
	{
		if (triangleVertices[0] == vertices[i] && triangleColors[0] == colors[i] && triangleNormals[0] == normals[i] && triangleUVs[0] == uvs[i])
		{
			index0 = i;
		}

		if (triangleVertices[1] == vertices[i] && triangleColors[1] == colors[i] && triangleNormals[1] == normals[i] && triangleUVs[1] == uvs[i])
		{
			index1 = i;
		}

		if (triangleVertices[2] == vertices[i] && triangleColors[2] == colors[i] && triangleNormals[2] == normals[i] && triangleUVs[2] == uvs[i])
		{
			index2 = i;
		}
	}


	if (index0 != 65535)
	{
		indices.push_back(index0);
	}
	else
	{
		vertices.push_back(triangleVertices[0]);
		colors.push_back(triangleColors[0]);
		normals.push_back(triangleNormals[0]);
		uvs.push_back(triangleUVs[0]);
		indices.push_back((unsigned short)(vertices.size() - 1));

		xMin = std::min(xMin, triangleVertices[0].x);
		xMax = std::max(xMax, triangleVertices[0].x);
		yMin = std::min(yMin, triangleVertices[0].y);
		yMax = std::max(yMax, triangleVertices[0].y);
		zMin = std::min(zMin, triangleVertices[0].z);
		zMax = std::max(zMax, triangleVertices[0].z);
	}

	if (index1 != 65535)
	{
		indices.push_back(index1);
	}
	else
	{
		vertices.push_back(triangleVertices[1]);
		colors.push_back(triangleColors[1]);
		normals.push_back(triangleNormals[1]);
		uvs.push_back(triangleUVs[1]);
		indices.push_back((unsigned short)(vertices.size() - 1));

		xMin = std::min(xMin, triangleVertices[1].x);
		xMax = std::max(xMax, triangleVertices[1].x);
		yMin = std::min(yMin, triangleVertices[1].y);
		yMax = std::max(yMax, triangleVertices[1].y);
		zMin = std::min(zMin, triangleVertices[1].z);
		zMax = std::max(zMax, triangleVertices[1].z);
	}

	if (index2 != 65535)
	{
		indices.push_back(index2);
	}
	else
	{
		vertices.push_back(triangleVertices[2]);
		colors.push_back(triangleColors[2]);
		normals.push_back(triangleNormals[2]);
		uvs.push_back(triangleUVs[2]);
		indices.push_back((unsigned short)(vertices.size() - 1));

		xMin = std::min(xMin, triangleVertices[2].x);
		xMax = std::max(xMax, triangleVertices[2].x);
		yMin = std::min(yMin, triangleVertices[2].y);
		yMax = std::max(yMax, triangleVertices[2].y);
		zMin = std::min(zMin, triangleVertices[2].z);
		zMax = std::max(zMax, triangleVertices[2].z);
	}
	Chunk::triangleCount++;
}


/**
 * Checks if chunk is inside frustum
 */
bool Chunk::isInsideFrustum(PlaneFrustum* pl)
{
	for (int i = 0; i < 6; i++)
	{

		glm::vec3 p = glm::vec3(xMin, yMin, zMin);
		if (pl->planes[i].n.x >= 0)
			p.x = xMax;
		if (pl->planes[i].n.y >= 0)
			p.y = yMax;
		if (pl->planes[i].n.z >= 0)
			p.z = zMax;

		glm::vec3 n = glm::vec3(xMax, yMax, zMax);
		if (pl->planes[i].n.x >= 0)
			n.x = xMin;
		if (pl->planes[i].n.y >= 0)
			n.y = yMin;
		if (pl->planes[i].n.z >= 0)
			n.z = zMin;

		if (pl->planes[i].distance(p) < 0)
			return false;
	}
	return true;
}

/**
 * Renders chunk
 */
void Chunk::render()
{
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // glm::normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // glm::normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		2,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // glm::normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	if (hasTexture)
	{
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			3,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // glm::normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		GLuint hasTex = glGetUniformLocation(Chunk::programID, "hasTexture");

		glUniform1i(hasTex, 1);

		GLuint texLocation = glGetUniformLocation(Chunk::programID, "textureSampler");
		GLuint lightMapLocation = glGetUniformLocation(Chunk::programID, "lightMapSampler");
		glUniform1i(texLocation, 0);
		glUniform1i(lightMapLocation, 1);

		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textures[0]);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, textures[1]);

	}
	else
	{


		GLuint hasTex = glGetUniformLocation(Chunk::programID, "hasTexture");

		glUniform1i(hasTex, 0);
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	//Draw the map
	glDrawElements(GL_TRIANGLES, sizeOfIndex, GL_UNSIGNED_SHORT, (void*)0);
}