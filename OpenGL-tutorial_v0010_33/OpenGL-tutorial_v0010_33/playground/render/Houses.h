#pragma once

#include <vector>
#include <iostream>

#include "ChunkCollection.h"
#include "House.h"
#include "../generation/Plot.h"

class Houses : public ChunkCollection
{
public:
	void generateTextures();
	void generateChunks(std::vector<Plot> &plots);

};