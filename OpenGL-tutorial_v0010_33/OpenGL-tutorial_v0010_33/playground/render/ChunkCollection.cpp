#include "ChunkCollection.h"
#include "common/controls.hpp"

/**
 * Renders all chunks in a collection
 */
void ChunkCollection::render()
{
	PlaneFrustum pl = getPlaneFrustum();
	for (int i = 0; i < chunkList.size(); i++)
	{
		if (chunkList[i]->isInsideFrustum(&pl))
		{
			chunkList[i]->render();
		}
	}
}