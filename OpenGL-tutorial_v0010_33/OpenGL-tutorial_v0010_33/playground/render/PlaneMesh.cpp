#include <string>

#include "PlaneMesh.h"
#include "PlaneChunk.h"
#include "../generation/GenerationParameters.h"
#include "../io/ConsolePrinter.h"

/**
 *	Generates mesh from grid
 */
void PlaneMesh::generateChunks(std::vector<std::vector<GridElement>> &grid)
{

	int chunkSize = ceil(FINEGRIDWIDTH / 100.0f);
	Chunk* tempChunk;
	for (int i = 0; i < 100 * 100; i++)
	{
		tempChunk = new PlaneChunk(grid, i, chunkSize, 100);
		if (tempChunk->size() > 0)
		{
			chunkList.push_back(tempChunk);
		}
		else
		{
			delete tempChunk;
		}

		ConsolePrinter::printProgress("Making plane mesh...", round(((float)i / 10000)*100.0f));

	}

	ConsolePrinter::printProgress("Making plane mesh...", 100);
	ConsolePrinter::progressFinished();
	ConsolePrinter::printString("Done!");

}