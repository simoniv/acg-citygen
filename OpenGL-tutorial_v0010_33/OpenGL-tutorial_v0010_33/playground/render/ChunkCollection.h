#pragma once

#include <deque>
#include "Chunk.h"

class ChunkCollection
{
protected:
	std::deque<Chunk*> chunkList;
public:
	//virtual void generateChunks(std::vector<std::vector<GridElement>> &grid) =0;
	void render();
};