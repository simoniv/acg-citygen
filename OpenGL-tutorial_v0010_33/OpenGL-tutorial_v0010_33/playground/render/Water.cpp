#include "Water.h"

/**
 * Create water object
 */
Water::Water(float minX, float minY, float maxX, float maxY)
{
	std::vector<glm::vec3> tempVerts;
	std::vector<glm::vec3> tempCols;
	std::vector<glm::vec3> tempNorms;

	glm::vec3 topLeft = glm::vec3(minX, -4.1f, minY);

	glm::vec3 topRight = glm::vec3(maxX, -4.1f, minY);

	glm::vec3 botLeft = glm::vec3(minX, -4.1f, maxY);

	tempVerts.push_back(topLeft);
	tempVerts.push_back(botLeft);
	tempVerts.push_back(topRight);

	tempCols.push_back(glm::vec3(0.0f, 0.0f, 0.7f));
	tempCols.push_back(glm::vec3(0.0f, 0.0f, 0.7f));
	tempCols.push_back(glm::vec3(0.0f, 0.0f, 0.7f));

	glm::vec3 edge1 = (topLeft)-(botLeft);
	glm::vec3 edge2 = (topRight)-(botLeft);
	glm::vec3 normal = glm::cross(edge2, edge1);
	glm::normalize(normal);

	tempNorms.push_back(normal);
	tempNorms.push_back(normal);
	tempNorms.push_back(normal);

	addTriangle(tempVerts, tempCols, tempNorms);

	tempVerts.clear();
	tempCols.clear();
	tempNorms.clear();

	glm::vec3 botRight = glm::vec3(maxX, -4.1f, maxY);

	tempVerts.push_back(topRight);
	tempVerts.push_back(botLeft);
	tempVerts.push_back(botRight);

	tempCols.push_back(glm::vec3(0.0f, 0.0f, 0.7f));
	tempCols.push_back(glm::vec3(0.0f, 0.0f, 0.7f));
	tempCols.push_back(glm::vec3(0.0f, 0.0f, 0.7f));

	edge1 = (topRight)-(botRight);
	edge2 = (botLeft)-(botRight);
	normal = glm::cross(edge1, edge2);
	glm::normalize(normal);

	tempNorms.push_back(normal);
	tempNorms.push_back(normal);
	tempNorms.push_back(normal);

	addTriangle(tempVerts, tempCols, tempNorms);

	hasTexture = false;

	generateVBO();
}